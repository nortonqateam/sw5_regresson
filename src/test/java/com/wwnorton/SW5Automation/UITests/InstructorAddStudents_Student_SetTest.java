package com.wwnorton.SW5Automation.UITests;

/**
 * @author Shripad
 *
 * 26-Sep-2019
 */
import java.util.ArrayList;
import org.openqa.selenium.By;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;

import com.wwnorton.SW5Automation.objectFactory.CreateSW5_TrialAccessUser;
import com.wwnorton.SW5Automation.objectFactory.Create_New_Student_Set_Page;
import com.wwnorton.SW5Automation.objectFactory.ManageStudentSetsPage;
import com.wwnorton.SW5Automation.objectFactory.UpdateStudentSet;
import com.wwnorton.SW5Automation.utilities.PropertiesFile;
import com.wwnorton.SW5Automation.utilities.TestListener;


//Call to TestNG listeners to save test logs and attachments as screen shots.
@Listeners({TestListener.class })
public class InstructorAddStudents_Student_SetTest extends PropertiesFile  {
	CreateSW5_TrialAccessUser NewStudent;
	LoginAsInstructorTest logininst;
	UpdateStudentSet updatestudentset;
	Create_New_Student_Set_Page createStudentset;
	ManageStudentSetsPage managestudentsetpage;
	public String StudentName;
	ArrayList<String> StudentsName = new ArrayList<String>();
	/**
	 * @throws Exception
	 */
	int i;
	@Parameters("Browser")
	@BeforeTest
	public void setUp () throws Exception 
	{
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig();
		PropertiesFile.setURL();
        
    }
	
	@Severity(SeverityLevel.NORMAL)
	@Description("As an Instructor -  Take SS ID and add a students to student set. These should be all trial users.")
	@Stories("AS-11 As an Instructor - Add Students to the Student Set. ")
	@Test
	public void CreateNewStudents() throws Exception{
		/**Create Students */
		NewStudent = new CreateSW5_TrialAccessUser(driver);
		//Add the student object to a list 
		for(i=1; i<=1; i++){
		NewStudent.Login_Smartwork5();
		StudentName =NewStudent.expectedUserName.toString();
		
		StudentsName.add(StudentName);
		Thread.sleep(2000);
		}
		String [] str = GetStringArray(StudentsName);
		
		/**Calling Create Student set method */
		logininst = new LoginAsInstructorTest();
		logininst.loginInstructor();
		createStudentset = new Create_New_Student_Set_Page(driver);
		createStudentset.SignIn_Register();
		createStudentset.ManageStudentSetlink();
		createStudentset.CreateStudentSetButton();
		createStudentset.createNewStudentset();
		createStudentset.createStudentset_information();
		String StudentID  = createStudentset.createStudentset_ID();
		
		/** Search Student by Student ID  */
		Thread.sleep(5000);
		managestudentsetpage = new ManageStudentSetsPage(driver);

		managestudentsetpage.Search_Text.click();
		driver.findElement(By.xpath("//input[@type='search']")).sendKeys(StudentID);
		managestudentsetpage.Updatebutton.click();
		updatestudentset =new UpdateStudentSet(driver);
		for (int j=0; j<StudentsName.size(); j++){
		updatestudentset.updateStudent();
		updatestudentset.EmailAddressTextbox.sendKeys(str[j]);
		updatestudentset.LookupButton.click();
		Thread.sleep(2000);
		updatestudentset.AddButton.click();
		Thread.sleep(5000);
		/** get the StudentNames from Members section */
		updatestudentset.VerifyStudentinfo();
		/** compare the Students Name */
		//Assert.assertEquals("The Added studentName are matching " + studentsname , StudentsName);
		}
		/** Click the Save button from Member Section  */
		updatestudentset.SaveButton();
		
	}
	
	public static String[] GetStringArray(ArrayList<String> arr) 
    { 
  
        // declaration and initialise String Array 
        String str[] = new String[arr.size()]; 
  
        // ArrayList to Array Conversion 
        for (int j = 0; j < arr.size(); j++) { 
  
            // Assign each value to String array 
            str[j] = arr.get(j); 
        } 
  
        return str; 
    } 
	
	// Closing driver and test.
	
		@AfterTest	
		public void closeTest() throws Exception {
			
			PropertiesFile.tearDownTest();
		
		}
}
