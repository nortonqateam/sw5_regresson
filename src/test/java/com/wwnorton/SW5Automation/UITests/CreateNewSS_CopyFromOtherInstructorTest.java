package com.wwnorton.SW5Automation.UITests;

import java.util.NoSuchElementException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.wwnorton.SW5Automation.objectFactory.CreateNewStudentSet;
import com.wwnorton.SW5Automation.objectFactory.LoginPage;
import com.wwnorton.SW5Automation.objectFactory.SW5DLPPage;
import com.wwnorton.SW5Automation.utilities.PropertiesFile;
import com.wwnorton.SW5Automation.utilities.ReadUIJsonFile;
import com.wwnorton.SW5Automation.utilities.ReusableMethods;
import com.wwnorton.SW5Automation.utilities.TestListener;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;



//Call to TestNG listeners to save test logs and attachments as screen shots.
@Listeners({TestListener.class })


public class CreateNewSS_CopyFromOtherInstructorTest extends PropertiesFile {

	ReadUIJsonFile readJasonObject = new ReadUIJsonFile();
	JsonObject jsonObj = readJasonObject.readUIJason();
 
    /*
    	Do setup operations, get JSON response from the API and put it into JsonPath object
    	Then we will do query and manipulations with JsonPath class’s methods.
    */
    
	// TestNG Annotations
	@Parameters("Browser")
	@BeforeTest
	
	//Test Setup
    public void setUp () throws Exception 
	{
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig();
		PropertiesFile.setURL();
        
    }
 
	
	// Allure  Annotations

	@Severity(SeverityLevel.CRITICAL)
	@Description("Create New Student Set copying Student set from Other Instructor")
	@Stories("Create Student Set_Copy From Other Instructor")
	@Test
	
	public void createNewStudentSetCopyFromOtherInstructor() throws Exception 
	{	
		String instructorUserName = jsonObj.getAsJsonObject("InstructorLoginCredentials").get("userName").getAsString();
		String instructorPassword = jsonObj.getAsJsonObject("InstructorLoginCredentials").get("password").getAsString();
		
		String JsonSourceSS = jsonObj.getAsJsonObject("CreateStudentSet_CopyFromOtherInstructor").get("sourceStudentSet").getAsString();
		String JsonSSID	= jsonObj.getAsJsonObject("CreateStudentSet_CopyFromOtherInstructor").get("copyStudentSetId").getAsString();
		String JsonStudentSetTitle = jsonObj.getAsJsonObject("CreateStudentSet_CopyFromOtherInstructor").get("studentSetTitle").getAsString();
		String JsonSchoolState = jsonObj.getAsJsonObject("CreateStudentSet_CopyFromOtherInstructor").get("schoolState").getAsString();
		String JsonSchoolName = jsonObj.getAsJsonObject("CreateStudentSet_CopyFromOtherInstructor").get("schoolName").getAsString();
		String JsonSSStartDate = jsonObj.getAsJsonObject("CreateStudentSet_CopyFromOtherInstructor").get("startDate").getAsString();
		String JsonSSEndDate = jsonObj.getAsJsonObject("CreateStudentSet_CopyFromOtherInstructor").get("endDate").getAsString();

		LoginPage login = PageFactory.initElements(driver, LoginPage.class);
		login.loginSW5(instructorUserName, instructorPassword);
			
		CreateNewStudentSet createSS = PageFactory.initElements(driver, CreateNewStudentSet.class);
		createSS.manageStudentSet();
		Thread.sleep(5000);
		createSS.clickCreateNewStudentSet();
		Thread.sleep(5000);
		createSS.createNewStudentSetWithCopyOtherInstructor(JsonSourceSS, JsonSSID);
		createSS.studentSetInformationCopy(JsonStudentSetTitle, JsonSchoolState, JsonSchoolName, JsonSSStartDate, JsonSSEndDate);
		Thread.sleep(5000);
		String NewStudentSetConfirmation = createSS.confirmNewStudentSet();
		Assert.assertEquals(NewStudentSetConfirmation, JsonStudentSetTitle);
		
		createSS.closeManageStudentSetPopUp();
		Thread.sleep(5000);
		login.clickSW5Icon();
		Thread.sleep(5000);
		SW5DLPPage dlp = PageFactory.initElements(driver, SW5DLPPage.class);
		dlp.selectSSByValue("0");
		int count = dlp.assignmentRowCount();
		
		ReusableMethods.checkPageIsReady(PropertiesFile.driver);
		
		//Validating Number of Assignments with JSON Response.
		String JSONCount = jsonObj.getAsJsonObject("CreateStudentSet_CopyFromOtherInstructor").get("numberOfAssignment").getAsString();
		Assert.assertEquals(String.valueOf(count), JSONCount);
		
		
		//Verify that all assignment titles in UI are matching exactly with JSON - assignment titles.
		JsonObject jsonResObj = jsonObj.getAsJsonObject("CreateStudentSet_CopyFromOtherInstructor");
		JsonArray jsonArrayObj = jsonResObj.getAsJsonArray("sourceAssignment");
		
		JsonObject jsonTitleObj = new JsonObject();
		
		WebElement AssignmentTitle;
		int matchCounter = 0;
		
		Thread.sleep(3000);
		
		try {	
			for (int i=1; i<=count; i++)
			{

					AssignmentTitle = driver.findElement(By.xpath("//*[@id=\"activity_list_table\"]/tbody/tr[" + i + "]/td[1]/a"));
					boolean IsAssignmentPublished = AssignmentTitle.isDisplayed();
					
					if(IsAssignmentPublished == true) {
						
						for (int j=0; j<count; j++) 
						{
							jsonTitleObj = jsonArrayObj.get(j).getAsJsonObject();
							String jsonStrTitle = jsonTitleObj.get("title").getAsString();
				            String AssignmentTitleText =AssignmentTitle.getText();
							if(AssignmentTitleText.equals(jsonStrTitle))
							{
								matchCounter++;
								break;
							}
						}
				
					}
					
				}  
				
			} catch (Exception e) {
				
				throw new NoSuchElementException("Assignment is NOT published yet: " + e.getMessage());

		}

		Assert.assertEquals(matchCounter, count, "All UI Assignment Titles are matching correctly?");
		
			
		//Verify that the GAU date is matching with JSON - GAU Date.
			WebElement gauDate;
			WebElement gauAssignmentTitle;
			
			try {
				
				for (int i=1; i<count; i++) 
				{	
					gauAssignmentTitle = driver.findElement(By.xpath("//*[@id=\"activity_list_table\"]/tbody/tr[" + i + "]/td[1]/a"));
					boolean IsAssignmentPublished = gauAssignmentTitle.isDisplayed();
					
					if (IsAssignmentPublished == true)
					{
						gauDate = driver.findElement(By.xpath("//*[@id=\"activity_list_table\"]/tbody/tr[" + i + "]/td[3]"));
						String strGAUDate = gauDate.getText(); 
						String strAssignmentTitle = gauAssignmentTitle.getText();

						for (int j=0; j<count; j++)
						{	
					
							jsonTitleObj = jsonArrayObj.get(j).getAsJsonObject();
							String jsonGAUDate = jsonTitleObj.get("gau").getAsString();
							String jsonStrTitle = jsonTitleObj.get("title").getAsString();
					
							if(!strGAUDate.equals("—") && strAssignmentTitle.equalsIgnoreCase(jsonStrTitle))
							{
								Assert.assertEquals(strGAUDate, jsonGAUDate , "Assignment with GAU Date: " + " " + jsonStrTitle);
								System.out.println("Assignment with GAU in UI matches with JSON - Assignment with GAU: " + gauAssignmentTitle.getText());
								break;
							}
						
						}
					}
		
				}
			} catch (Exception e) {
				
				throw new NoSuchElementException("Assignment is NOT published yet: " + e.getMessage()); 
				
		}
			 
		 
		//Verify that the Adaptive assignment in UI is matching with JSON - Adaptive value.
			WebElement adptiveTitle;
			
			try {
				for (int i=1; i<count; i++) 
				{
					adptiveTitle = driver.findElement(By.xpath("//*[@id=\"activity_list_table\"]/tbody/tr[" + i + "]/td[1]/a"));
					boolean IsAssignmentPublished = adptiveTitle.isDisplayed();
					boolean isAdaptive = ReusableMethods.elementExist(PropertiesFile.driver, "//*[@id=\"activity_list_table\"]/tbody/tr[" + i + "]/td[1]/img");
					
					if(IsAssignmentPublished == true)
					{
						for (int j=0; j<count; j++)
						{	
							jsonTitleObj = jsonArrayObj.get(j).getAsJsonObject();
							String jsonAdaptiveStrValue = jsonTitleObj.get("isAdaptive").getAsString();
							String jsonStrTitle = jsonTitleObj.get("title").getAsString();
				 
							if(isAdaptive && adptiveTitle.getText().equals(jsonStrTitle))
								{		
									Assert.assertEquals(jsonAdaptiveStrValue, "true");
									System.out.println("Adaptive Assignment in UI matches with JSON - Adaptive: " + adptiveTitle.getText());	
					
								}
				
							else if(!isAdaptive && adptiveTitle.getText().equals(jsonStrTitle)) 
								{
					
									Assert.assertEquals(jsonAdaptiveStrValue, "false");
						
								}
							}
					
						}
	
					}
				} catch (Exception e) {
			
			throw new NoSuchElementException("Assignment is NOT published yet: " + e.getMessage()); 
			
	}
			
		 
		login.logoutSW5();
	

	}
	
	
	// Closing driver and test.
	
	@AfterTest	
	public void closeTest() throws Exception {
		
		PropertiesFile.tearDownTest();
	
	}
	
}
