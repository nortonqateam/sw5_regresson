/**
 * 
 */
package com.wwnorton.SW5Automation.UITests;

import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;

import com.google.gson.JsonObject;
import com.wwnorton.SW5Automation.objectFactory.AssignmentPage;
import com.wwnorton.SW5Automation.objectFactory.Create_New_Student_Set_Page;
import com.wwnorton.SW5Automation.objectFactory.LoginPage;
import com.wwnorton.SW5Automation.objectFactory.ManageStudentSetsPage;
import com.wwnorton.SW5Automation.objectFactory.Questions_Page;
import com.wwnorton.SW5Automation.objectFactory.SW5DLPPage;
import com.wwnorton.SW5Automation.utilities.PropertiesFile;
import com.wwnorton.SW5Automation.utilities.ReadUIJsonFile;
import com.wwnorton.SW5Automation.utilities.TestListener;

/**
 * @author Shripad
 *
 * 26-Sep-2019
 */

//Call to TestNG listeners to save test logs and attachments as screen shots.
@Listeners({TestListener.class })
public class CreateFollowUp_PartialAdaptive_AssignmentFromScratchTest extends PropertiesFile {
	LoginAsInstructorTest logininst;
	Create_New_Student_Set_Page createStudentset;
	ManageStudentSetsPage managestudentsetpage;
	LoginPage DLPpage;
	SW5DLPPage SW5DLPpage;
	AssignmentPage assignmentpage;
	Questions_Page questionspage;
	
	ReadUIJsonFile readJasonObject = new ReadUIJsonFile();
	JsonObject jsonObj = readJasonObject.readUIJason();	
	
	@Parameters("Browser")
	@BeforeTest
	public void setUp () throws Exception 
	{
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig();
		PropertiesFile.setURL();
        
    }
	
	@Severity(SeverityLevel.NORMAL)
	@Description("As an Instructor - Create Warm Up (Partial Adaptive) Assignment from scratch.")
	@Stories("AS-13 As an Instructor - Create Warm Up (Partial Adaptive) Assignment from scratch with assignment questions, policy and learning objectives. ")
	@Test
	public void CreateWarmUpPartialAdaptiveAssignment() throws Exception{
		//Login as Instructor 
		logininst = new LoginAsInstructorTest();
		logininst.loginInstructor();
		//Click the Manage Student Set Link and Create new Student 
		createStudentset = new Create_New_Student_Set_Page(driver);
		createStudentset.SignIn_Register();
		createStudentset.ManageStudentSetlink();
		createStudentset.CreateStudentSetButton();
		createStudentset.createNewStudentset();
		createStudentset.createStudentset_information();
		String StudentID  = createStudentset.createStudentset_ID();
		System.out.println(StudentID);
		//Close the Manage Student Set page and navigate to Smartwork5 page
		managestudentsetpage = new ManageStudentSetsPage(driver);
		managestudentsetpage.clickcloselink();
		DLPpage = new LoginPage(driver);
		Thread.sleep(2000);
		DLPpage.clickSW5Icon();
		Thread.sleep(2000);
		//driver.findElement(By.xpath("//select[@id='report_class_menu']")).click();
		SW5DLPpage = new SW5DLPPage(driver);
		SW5DLPpage.selectByPartOfVisibleText(StudentID);
		Thread.sleep(8000);
		SW5DLPpage.ClickAssignment();
		String ChildWindow = null;
	    Set<String> Windowhandles= driver.getWindowHandles();
	    String ParentWindow = driver.getWindowHandle();
	    Windowhandles.remove(ParentWindow);
	    String winHandle=Windowhandles.iterator().next();
	    if (winHandle!=ParentWindow){
	    	ChildWindow=winHandle;
	    }
	    driver.switchTo().window(ChildWindow);
		Thread.sleep(8000);
		driver.switchTo().frame("swfb_iframe");
		WebDriverWait wait = new WebDriverWait(driver, 50);
		 
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[@class='instructor-intro-assignment-title']")));
		//String Data =driver.findElement(By.xpath("//span[@class='instructor-intro-assignment-title']")).getText();
		//System.out.println(Data);
		assignmentpage = new AssignmentPage(driver);
		assignmentpage.editAssignmentbutton();
		String Assignmentname = jsonObj.getAsJsonObject("FollowUp_PartialAdaptiveInfo").get("FollowUp_PartialAdaptiveAssignemntName").getAsString();
		assignmentpage.enterAssignmentName(Assignmentname);
		assignmentpage.gauDate();
		assignmentpage.adaptiveOn();
		assignmentpage.followUp();
		
		questionspage = new Questions_Page(driver);
		questionspage.addQuestions();
		Thread.sleep(5000);
		//driver.switchTo().frame("swfb_iframe");
		//WebElement TypeListBox= wait1.until(ExpectedConditions.presenceOfElementLocated((By) questionspage.TypeListBox));
		//driver.findElement(By.xpath("//input[@type='text']")).sendKeys("Test");
		questionspage.SelectTypeListbox();
		questionspage.headTitleLink();
		wait = new WebDriverWait(driver,8);
		TimeUnit.SECONDS.sleep(8);
		wait.until(ExpectedConditions.visibilityOf(assignmentpage.SELECTOBJECTIVES));
		assignmentpage.selectObejctive();
		assignmentpage.saveButton();
		Thread.sleep(5000);
		assignmentpage.publishButton();
		questionspage.headTitleLink();
		assignmentpage.warmUp_Questions();
		driver.switchTo().window(ParentWindow);
		wait = new WebDriverWait(driver,8);
		TimeUnit.SECONDS.sleep(8);
		assignmentpage.Assignmentlinks(Assignmentname);
	}
	@AfterTest	
	public void closeTest() throws Exception {
		
		PropertiesFile.tearDownTest();
	
	}
	
}
