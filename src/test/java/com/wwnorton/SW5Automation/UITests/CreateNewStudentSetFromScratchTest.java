package com.wwnorton.SW5Automation.UITests;

import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.wwnorton.SW5Automation.utilities.TestListener;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;

import com.wwnorton.SW5Automation.objectFactory.LoginPage;
import com.google.gson.JsonObject;
import com.wwnorton.SW5Automation.objectFactory.CreateNewStudentSet;
import com.wwnorton.SW5Automation.utilities.PropertiesFile;
import com.wwnorton.SW5Automation.utilities.ReadUIJsonFile;


//Call to TestNG listeners to save test logs and attachments as screen shots.

@Listeners({TestListener.class })


public class CreateNewStudentSetFromScratchTest extends PropertiesFile {
	
	
	ReadUIJsonFile readJasonObject = new ReadUIJsonFile();
	JsonObject jsonObj = readJasonObject.readUIJason();
	
	
	// TestNG Annotations
	@Parameters("Browser")
	@BeforeTest
	
	
	// Call to Properties file initiate Browser and set Test URL.
	
		public void callPropertiesFile() throws Exception {
		
			PropertiesFile.readPropertiesFile();
			PropertiesFile.setBrowserConfig();
			PropertiesFile.setURL();
		
		}

//	Validate SW5 Instructor login functionality. 
	
	// Allure  Annotations

	@Severity(SeverityLevel.CRITICAL)
	@Description("Create New Student Set from Scratch")
	@Stories("Create New Student Set")
	@Test
	
		public void createNewStudentSetFromScratch() throws Exception {
		
			String instructorUserName = jsonObj.getAsJsonObject("InstructorLoginCredentials").get("userName").getAsString();
			String instructorPassword = jsonObj.getAsJsonObject("InstructorLoginCredentials").get("password").getAsString();
			
			String JsonStudentSetTitle = jsonObj.getAsJsonObject("CreateNewStudentSetFromScratch").get("studentSetTitle").getAsString();
			String JsonSchoolState = jsonObj.getAsJsonObject("CreateNewStudentSetFromScratch").get("schoolState").getAsString();
			String JsonSchoolName = jsonObj.getAsJsonObject("CreateNewStudentSetFromScratch").get("schoolName").getAsString();
			String JsonSSStartDate = jsonObj.getAsJsonObject("CreateNewStudentSetFromScratch").get("startDate").getAsString();
			String JsonSSEndDate = jsonObj.getAsJsonObject("CreateNewStudentSetFromScratch").get("endDate").getAsString();
			
	
			LoginPage login = PageFactory.initElements(driver, LoginPage.class);
			login.loginSW5(instructorUserName, instructorPassword);
			Thread.sleep(3000);
			login.clickSW5Icon();
			Thread.sleep(3000);
			CreateNewStudentSet createSS = PageFactory.initElements(driver, CreateNewStudentSet.class);
			createSS.manageStudentSet();
			Thread.sleep(3000);
			createSS.clickCreateNewStudentSet();
			createSS.createNewStudentSetFromScratch(JsonStudentSetTitle, JsonSchoolState, JsonSchoolName, JsonSSStartDate, JsonSSEndDate);
			
			String NewStudentSetConfirmation = createSS.confirmNewStudentSet();
			Assert.assertEquals(NewStudentSetConfirmation, JsonStudentSetTitle);
			
			createSS.closeManageStudentSetPopUp();
			
			login.logoutSW5();

		}
	
	
	// Closing driver and test.
	
	@AfterTest	
	public void closeTest() throws Exception {
		
	PropertiesFile.tearDownTest();
	
	}

}
