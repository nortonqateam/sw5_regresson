/**
 * 
 */
package com.wwnorton.SW5Automation.UITests;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;

import com.wwnorton.SW5Automation.objectFactory.AssignmentPage;
import com.wwnorton.SW5Automation.objectFactory.Create_New_Student_Set_Page;
import com.wwnorton.SW5Automation.objectFactory.LoginPage;
import com.wwnorton.SW5Automation.objectFactory.ManageStudentSetsPage;
import com.wwnorton.SW5Automation.objectFactory.SW5DLPPage;
import com.wwnorton.SW5Automation.utilities.LogUtil;
import com.wwnorton.SW5Automation.utilities.PropertiesFile;
import com.wwnorton.SW5Automation.utilities.TestListener;

/**
 * @author Shripad
 *
 * 15-Oct-2019
 */
@Listeners({TestListener.class })

public class InstructorCreatePremadeAssignmentsTest extends PropertiesFile {
	
	LoginAsInstructorTest logininst;
	LoginPage DLPpage;
	Create_New_Student_Set_Page createStudentset;
	ManageStudentSetsPage managestudentsetpage;
	SW5DLPPage SW5DLPpage;
	AssignmentPage assignmentpage;
	String StudentID;
	JavascriptExecutor js = (JavascriptExecutor) driver;
	
	
	@Parameters("Browser")
	@BeforeTest
	public void setUp () throws Exception 
	{
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig();
		PropertiesFile.setURL();
        
    }
	
	@Severity(SeverityLevel.NORMAL)
	@Description("CH1HW (Assignment with GAU & Set the GAU to +60 mins of current date & time)")
	@Stories("AS-42 As an instructor -  As an instructor use pre-made assignment with different settings combination")
	@Test(priority=0)
	public void CreatePremadeAssignmentCH1HW(ITestContext context) throws Exception{
		logininst = new LoginAsInstructorTest();
		logininst.loginInstructor();
		createStudentset = new Create_New_Student_Set_Page(driver);
		createStudentset.SignIn_Register();
		createStudentset.ManageStudentSetlink();
		createStudentset.CreateStudentSetButton();
		createStudentset.createNewStudentset();
		createStudentset.createStudentset_information();
        StudentID  = createStudentset.createStudentset_ID();
		System.out.println(StudentID);
		context.setAttribute("StudentIDAttribute", StudentID);
		managestudentsetpage = new ManageStudentSetsPage(driver);
		managestudentsetpage.clickcloselink();
		DLPpage = new LoginPage(driver);
		Thread.sleep(1000);
		DLPpage.clickSW5Icon();
		Thread.sleep(1000);
		SW5DLPpage = new SW5DLPPage(driver);
		SW5DLPpage.selectByPartOfVisibleText(StudentID);
		LogUtil.log("Student ID is " + StudentID);
		Thread.sleep(5000);
		SW5DLPpage.ClickAssignmentlink("CH1HW");
		
		String winHandleBefore  = driver.getWindowHandle();
		for(String winHandle : driver.getWindowHandles()){
			 driver.switchTo().window(winHandle);
		}
		Thread.sleep(8000);
		driver.switchTo().frame("swfb_iframe");
		wait = new WebDriverWait(driver, 60);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//span[@class='instructor-intro-assignment-title']")));
		assignmentpage = new AssignmentPage(driver);
		assignmentpage.editAssignmentbutton();
	
		WebElement currentdate =driver.findElement(By.xpath("//div[@class='timeZoneSpan']/span[@class='ant-calendar-picker']/span/input[@placeholder='mm/dd/yyyy']"));
		currentdate.click();
		//WebElement todaydate = driver.findElement(By.xpath("//table[@class='ant-calendar-table']/tbody[@class='ant-calendartbody']/tr/td[@class='ant-calendar-cell ant-calendar-today']"));
		//todaydate.click();
		Thread.sleep(2000);
		WebElement calendarInput =driver.findElement(By.xpath("//input[@class='ant-calendar-input  ']"));
		calendarInput.sendKeys("10/27/2019");
		//assignmentpage.selectGAUDate();
		String time2=null;
		assignmentpage.selectGAUTime(time2);
		assignmentpage.saveButton();
		Thread.sleep(5000);
		assignmentpage.publishButton();
		Thread.sleep(5000);
		//assignmentpage.AssignmentNameHeadTitle.click();
		String winHandleAfter = driver.getWindowHandle();
		driver.close();
		driver.switchTo().window(winHandleBefore);
		wait = new WebDriverWait(driver,8);
		TimeUnit.SECONDS.sleep(8);
		DLPpage.logoutSW5();
	}
	
	
	@Severity(SeverityLevel.NORMAL)
	@Description("CH4HW (Ungraded Practice: after attempt exhaust / answer is correct / student viewed solution)")
	@Test(priority=1)
	public void CreatePremadeAssignmentCH4HW(ITestContext context) throws Exception{
		logininst = new LoginAsInstructorTest();
		logininst.loginInstructor();
		DLPpage = new LoginPage(driver);
		Thread.sleep(2000);
		DLPpage.clickSW5Icon();
		SW5DLPpage = new SW5DLPPage(driver);
		Thread.sleep(2000);
		//System.out.println(StudentID);
		String StudentID_CH4HW = (String) context.getAttribute("StudentIDAttribute");
		//SW5DLPpage.selectByPartOfVisibleText("150970");
		LogUtil.log("Student ID is " + StudentID);
		Thread.sleep(5000);
		SW5DLPpage.ClickAssignmentlink("CH4HW");
		String winHandleBefore  = driver.getWindowHandle();
		for(String winHandle : driver.getWindowHandles()){
			 driver.switchTo().window(winHandle);
		}
		Thread.sleep(8000);
		driver.switchTo().frame("swfb_iframe");
		wait = new WebDriverWait(driver, 60);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//span[@class='instructor-intro-assignment-title']")));
		assignmentpage = new AssignmentPage(driver);
		assignmentpage.editAssignmentbutton();
	    Thread.sleep(3000);
		WebElement currentdate =driver.findElement(By.xpath("//div[@class='timeZoneSpan']/span[@class='ant-calendar-picker']/span/input[@placeholder='mm/dd/yyyy']"));
		currentdate.click();
		Thread.sleep(2000);
		//WebElement todaydate = driver.findElement(By.xpath("//table[@class='ant-calendar-table']/tbody[@class='ant-calendartbody']/tr/td[@class='ant-calendar-cell ant-calendar-today']"));
		//todaydate.click();
		WebElement calendarInput =driver.findElement(By.xpath("//input[@class='ant-calendar-input  ']"));
		calendarInput.sendKeys("10/27/2019");
		//assignmentpage.selectGAUDate();
		String time2=null;
		assignmentpage.selectGAUTime(time2);
		assignmentpage.ShowAdditionalSettingsButton();
		String ungradedPratice="After attempts are exhausted, answer is correct, or student has viewed the solution";
		assignmentpage.selectUngradedPractice(ungradedPratice);
		//WebElement Attempts = driver.findElement(By.xpath("//span[@class='ant-select-selection__rendered']"));
		((JavascriptExecutor)driver).executeScript("window.scrollBy(500,500)");
		assignmentpage.selectAttempts("3");
		assignmentpage. ApplyToAllButton.click();
		assignmentpage.saveButton();
		Thread.sleep(5000);
		assignmentpage.publishButton();
		Thread.sleep(5000);
		//assignmentpage.AssignmentNameHeadTitle.click();
		String winHandleAfter = driver.getWindowHandle();
		driver.close();
		driver.switchTo().window(winHandleBefore);
		wait = new WebDriverWait(driver,8);
		TimeUnit.SECONDS.sleep(8);
		DLPpage.logoutSW5();
	}
	
	@Severity(SeverityLevel.NORMAL)
	@Description("CH5HW (Ungraded Practice: after the GAU date pass)")
	@Test(priority=2)
	public void CreatePremadeAssignmentCH5HW(ITestContext context) throws Exception{
		logininst = new LoginAsInstructorTest();
		logininst.loginInstructor();
		DLPpage = new LoginPage(driver);
		Thread.sleep(2000);
		DLPpage.clickSW5Icon();
		SW5DLPpage = new SW5DLPPage(driver);
		Thread.sleep(2000);
		//System.out.println(StudentID);
		//String Customer_id1 = (String) context.getAttribute("StudentIDAttribute");
		String StudentID_CH5HW = (String) context.getAttribute("StudentIDAttribute");
		SW5DLPpage.selectByPartOfVisibleText(StudentID_CH5HW);
		LogUtil.log("Student ID is " + StudentID);
		Thread.sleep(3000);
		SW5DLPpage.ClickAssignmentlink("CH5HW");
		String winHandleBefore  = driver.getWindowHandle();
		for(String winHandle : driver.getWindowHandles()){
			 driver.switchTo().window(winHandle);
		}
		Thread.sleep(8000);
		driver.switchTo().frame("swfb_iframe");
		wait = new WebDriverWait(driver, 60);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//span[@class='instructor-intro-assignment-title']")));
		assignmentpage = new AssignmentPage(driver);
		assignmentpage.editAssignmentbutton();
	    Thread.sleep(3000);
		WebElement currentdate =driver.findElement(By.xpath("//div[@class='timeZoneSpan']/span[@class='ant-calendar-picker']/span/input[@placeholder='mm/dd/yyyy']"));
		currentdate.click();

		//WebElement todaydate = driver.findElement(By.xpath("//table[@class='ant-calendar-table']/tbody[@class='ant-calendartbody']/tr/td[@class='ant-calendar-cell ant-calendar-today']"));
		//todaydate.click();
		Thread.sleep(2000);
		WebElement calendarInput =driver.findElement(By.xpath("//input[@class='ant-calendar-input  ']"));
		calendarInput.sendKeys("10/27/2019");
		//assignmentpage.selectGAUDate();
		String time2=null;
		assignmentpage.selectGAUTime(time2);
		assignmentpage.ShowAdditionalSettingsButton();
		String ungradedPratice="After the \"Grades Accepted Until\" date passes";
		assignmentpage.selectUngradedPractice(ungradedPratice);
		//WebElement Attempts = driver.findElement(By.xpath("//span[@class='ant-select-selection__rendered']"));
		assignmentpage.saveButton();
		Thread.sleep(5000);
		assignmentpage.publishButton();
		Thread.sleep(5000);
		//assignmentpage.AssignmentNameHeadTitle.click();
		String winHandleAfter = driver.getWindowHandle();
		driver.close();
		driver.switchTo().window(winHandleBefore);
		wait = new WebDriverWait(driver,8);
		TimeUnit.SECONDS.sleep(8);
		DLPpage.logoutSW5();
	}
	
	
	
	@Severity(SeverityLevel.NORMAL)
	@Description("CH6HW (Ungraded Practice: Never)")
	@Test(priority=3)
	public void CreatePremadeAssignmentCH6HW(ITestContext context) throws Exception{
		logininst = new LoginAsInstructorTest();
		logininst.loginInstructor();
		DLPpage = new LoginPage(driver);
		Thread.sleep(2000);
		DLPpage.clickSW5Icon();
		SW5DLPpage = new SW5DLPPage(driver);
		Thread.sleep(2000);
		//System.out.println(StudentID);
		
		String StudentID_CH6HW = (String) context.getAttribute("StudentIDAttribute");
		SW5DLPpage.selectByPartOfVisibleText(StudentID_CH6HW);
		LogUtil.log("Student ID is " + StudentID);
		Thread.sleep(3000);
		SW5DLPpage.ClickAssignmentlink("CH6HW");
		String winHandleBefore  = driver.getWindowHandle();
		for(String winHandle : driver.getWindowHandles()){
			 driver.switchTo().window(winHandle);
		}
		Thread.sleep(8000);
		driver.switchTo().frame("swfb_iframe");
		wait = new WebDriverWait(driver, 60);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//span[@class='instructor-intro-assignment-title']")));
		assignmentpage = new AssignmentPage(driver);
		assignmentpage.editAssignmentbutton();
	    Thread.sleep(3000);
		WebElement currentdate =driver.findElement(By.xpath("//div[@class='timeZoneSpan']/span[@class='ant-calendar-picker']/span/input[@placeholder='mm/dd/yyyy']"));
		currentdate.click();
		Thread.sleep(2000);
		//WebElement todaydate = driver.findElement(By.xpath("//table[@class='ant-calendar-table']/tbody[@class='ant-calendartbody']/tr/td[@class='ant-calendar-cell ant-calendar-today']"));
		//todaydate.click();
		WebElement calendarInput =driver.findElement(By.xpath("//input[@class='ant-calendar-input  ']"));
		calendarInput.sendKeys("10/27/2019");
		//assignmentpage.selectGAUDate();
		String time2=null;
		assignmentpage.selectGAUTime(time2);
		assignmentpage.ShowAdditionalSettingsButton();
		String ungradedPratice="Never";
		assignmentpage.selectUngradedPractice(ungradedPratice);
		//WebElement Attempts = driver.findElement(By.xpath("//span[@class='ant-select-selection__rendered']"));
		assignmentpage.saveButton();
		Thread.sleep(5000);
		assignmentpage.publishButton();
		Thread.sleep(5000);
		//assignmentpage.AssignmentNameHeadTitle.click();
		String winHandleAfter = driver.getWindowHandle();
		driver.close();
		driver.switchTo().window(winHandleBefore);
		wait = new WebDriverWait(driver,8);
		TimeUnit.SECONDS.sleep(8);
		DLPpage.logoutSW5();
	}
	
	@Severity(SeverityLevel.NORMAL)
	@Description("CH7HW (Show solution: Any time)")
	@Test(priority=4)
	public void CreatePremadeAssignmentCH7HW(ITestContext context) throws Exception{
		logininst = new LoginAsInstructorTest();
		logininst.loginInstructor();
		DLPpage = new LoginPage(driver);
		Thread.sleep(2000);
		DLPpage.clickSW5Icon();
		SW5DLPpage = new SW5DLPPage(driver);
		Thread.sleep(2000);
		//System.out.println(StudentID);
		//String Customer_id1 = (String) context.getAttribute("StudentIDAttribute");
		String StudentID_CH7HW = (String) context.getAttribute("StudentIDAttribute");
		SW5DLPpage.selectByPartOfVisibleText(StudentID_CH7HW);
		LogUtil.log("Student ID is " + StudentID);
		Thread.sleep(3000);
		SW5DLPpage.ClickAssignmentlink("CH7HW");
		String winHandleBefore  = driver.getWindowHandle();
		for(String winHandle : driver.getWindowHandles()){
			 driver.switchTo().window(winHandle);
		}
		Thread.sleep(8000);
		driver.switchTo().frame("swfb_iframe");
		wait = new WebDriverWait(driver, 60);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//span[@class='instructor-intro-assignment-title']")));
		assignmentpage = new AssignmentPage(driver);
		assignmentpage.editAssignmentbutton();
	    Thread.sleep(3000);
		WebElement currentdate =driver.findElement(By.xpath("//div[@class='timeZoneSpan']/span[@class='ant-calendar-picker']/span/input[@placeholder='mm/dd/yyyy']"));
		currentdate.click();
		//WebElement todaydate = driver.findElement(By.xpath("//table[@class='ant-calendar-table']/tbody[@class='ant-calendartbody']/tr/td[@class='ant-calendar-cell ant-calendar-today']"));
		//todaydate.click();
		WebElement calendarInput =driver.findElement(By.xpath("//input[@class='ant-calendar-input  ']"));
		calendarInput.sendKeys("10/27/2019");
		//assignmentpage.selectGAUDate();
		String time2=null;
		assignmentpage.selectGAUTime(time2);
		assignmentpage.ShowAdditionalSettingsButton();
		String ShowSolution="Any time (students may 'give up' and view the solution)";
		assignmentpage.selectshowsolution(ShowSolution);
		//WebElement Attempts = driver.findElement(By.xpath("//span[@class='ant-select-selection__rendered']"));
		assignmentpage.saveButton();
		Thread.sleep(5000);
		assignmentpage.publishButton();
		Thread.sleep(5000);
		//assignmentpage.AssignmentNameHeadTitle.click();
		String winHandleAfter = driver.getWindowHandle();
		driver.close();
		driver.switchTo().window(winHandleBefore);
		wait = new WebDriverWait(driver,8);
		TimeUnit.SECONDS.sleep(8);
		DLPpage.logoutSW5();
	}
	
	@Severity(SeverityLevel.NORMAL)
	@Description("CH8HW (Show solution: Only after a correct answer, or all attempts are exhausted)")
	@Test(priority=5)
	public void CreatePremadeAssignmentCH8HW(ITestContext context) throws Exception{
		logininst = new LoginAsInstructorTest();
		logininst.loginInstructor();
		DLPpage = new LoginPage(driver);
		Thread.sleep(2000);
		DLPpage.clickSW5Icon();
		SW5DLPpage = new SW5DLPPage(driver);
		Thread.sleep(2000);
		//System.out.println(StudentID);
		//String Customer_id1 = (String) context.getAttribute("StudentIDAttribute");
		String StudentID_CH8HW = (String) context.getAttribute("StudentIDAttribute");
		SW5DLPpage.selectByPartOfVisibleText(StudentID_CH8HW);
		LogUtil.log("Student ID is " + StudentID);
		Thread.sleep(3000);
		SW5DLPpage.ClickAssignmentlink("CH8HW");
		String winHandleBefore  = driver.getWindowHandle();
		for(String winHandle : driver.getWindowHandles()){
			 driver.switchTo().window(winHandle);
		}
		Thread.sleep(8000);
		driver.switchTo().frame("swfb_iframe");
		wait = new WebDriverWait(driver, 60);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//span[@class='instructor-intro-assignment-title']")));
		assignmentpage = new AssignmentPage(driver);
		assignmentpage.editAssignmentbutton();
	    Thread.sleep(3000);
		WebElement currentdate =driver.findElement(By.xpath("//div[@class='timeZoneSpan']/span[@class='ant-calendar-picker']/span/input[@placeholder='mm/dd/yyyy']"));
		currentdate.click();
		Thread.sleep(2000);
		//WebElement todaydate = driver.findElement(By.xpath("//table[@class='ant-calendar-table']/tbody[@class='ant-calendartbody']/tr/td[@class='ant-calendar-cell ant-calendar-today']"));
		//todaydate.click();
		WebElement calendarInput =driver.findElement(By.xpath("//input[@class='ant-calendar-input  ']"));
		calendarInput.sendKeys("10/27/2019");
		//assignmentpage.selectGAUDate();
		String time2=null;
		assignmentpage.selectGAUTime(time2);
		assignmentpage.ShowAdditionalSettingsButton();
		String ShowSolution="Only after a correct answer, or all attempts are exhausted";
		assignmentpage.selectshowsolution(ShowSolution);
		//WebElement Attempts = driver.findElement(By.xpath("//span[@class='ant-select-selection__rendered']"));
		assignmentpage.saveButton();
		Thread.sleep(5000);
		assignmentpage.publishButton();
		Thread.sleep(5000);
		//assignmentpage.AssignmentNameHeadTitle.click();
		String winHandleAfter = driver.getWindowHandle();
		driver.close();
		driver.switchTo().window(winHandleBefore);
		wait = new WebDriverWait(driver,8);
		TimeUnit.SECONDS.sleep(8);
		DLPpage.logoutSW5();
	}
	
	@Severity(SeverityLevel.NORMAL)
	@Description("CH9HW (Show solution: Only after the \"Grades Accepted Until\" date has passed)")
	@Test(priority=6)
	public void CreatePremadeAssignmentCH9HW(ITestContext context) throws Exception{
		logininst = new LoginAsInstructorTest();
		logininst.loginInstructor();
		DLPpage = new LoginPage(driver);
		Thread.sleep(2000);
		DLPpage.clickSW5Icon();
		SW5DLPpage = new SW5DLPPage(driver);
		Thread.sleep(2000);
		//System.out.println(StudentID);
		
		String StudentID_CH9HW = (String) context.getAttribute("StudentIDAttribute");
		SW5DLPpage.selectByPartOfVisibleText(StudentID_CH9HW);
		LogUtil.log("Student ID is " + StudentID);
		Thread.sleep(3000);
		SW5DLPpage.ClickAssignmentlink("CH9HW");
		Thread.sleep(3000);
		String winHandleBefore  = driver.getWindowHandle();
		for(String winHandle : driver.getWindowHandles()){
			 driver.switchTo().window(winHandle);
		}
		Thread.sleep(8000);
		driver.switchTo().frame("swfb_iframe");
		wait = new WebDriverWait(driver, 60);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//span[@class='instructor-intro-assignment-title']")));
		assignmentpage = new AssignmentPage(driver);
		assignmentpage.editAssignmentbutton();
	    Thread.sleep(3000);
		WebElement currentdate =driver.findElement(By.xpath("//div[@class='timeZoneSpan']/span[@class='ant-calendar-picker']/span/input[@placeholder='mm/dd/yyyy']"));
		currentdate.click();
		Thread.sleep(2000);
		//WebElement todaydate = driver.findElement(By.xpath("//table[@class='ant-calendar-table']/tbody[@class='ant-calendartbody']/tr/td[@class='ant-calendar-cell ant-calendar-today']"));
		//todaydate.click();
		WebElement calendarInput =driver.findElement(By.xpath("//input[@class='ant-calendar-input  ']"));
		calendarInput.sendKeys("10/27/2019");
		//assignmentpage.selectGAUDate();
		String time2=null;
		assignmentpage.selectGAUTime(time2);
		assignmentpage.ShowAdditionalSettingsButton();
		String ShowSolution="Only after the \"Grades Accepted Until\" date has passed";
		assignmentpage.selectshowsolution(ShowSolution);
		//WebElement Attempts = driver.findElement(By.xpath("//span[@class='ant-select-selection__rendered']"));
		assignmentpage.saveButton();
		Thread.sleep(5000);
		assignmentpage.publishButton();
		Thread.sleep(5000);
		//assignmentpage.AssignmentNameHeadTitle.click();
		String winHandleAfter = driver.getWindowHandle();
		driver.close();
		driver.switchTo().window(winHandleBefore);
		wait = new WebDriverWait(driver,8);
		TimeUnit.SECONDS.sleep(8);
		DLPpage.logoutSW5();
	}
	
	@Severity(SeverityLevel.NORMAL)
	@Description("CH10HW (Show Student Score In: Percentages")
	//@Test(priority=7)
	public void CreatePremadeAssignmentCH10HW(ITestContext context) throws Exception{
		logininst = new LoginAsInstructorTest();
		logininst.loginInstructor();
		DLPpage = new LoginPage(driver);
		Thread.sleep(2000);
		DLPpage.clickSW5Icon();
		SW5DLPpage = new SW5DLPPage(driver);
		Thread.sleep(2000);
		//System.out.println(StudentID);
		//String Customer_id1 = (String) context.getAttribute("StudentIDAttribute");
		String StudentID_CH10HW = (String) context.getAttribute("StudentIDAttribute");
		SW5DLPpage.selectByPartOfVisibleText(StudentID_CH10HW);
		LogUtil.log("Student ID is " + StudentID);
		Thread.sleep(3000);
		try {
			SW5DLPpage.ClickAssignmentlink("CH10HW");
		} catch (Exception e) {
			Assert.fail("Exception is occurred");
			e.printStackTrace();
		}
		String winHandleBefore  = driver.getWindowHandle();
		for(String winHandle : driver.getWindowHandles()){
			 driver.switchTo().window(winHandle);
		}
		Thread.sleep(8000);
		driver.switchTo().frame("swfb_iframe");
		wait = new WebDriverWait(driver, 60);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//span[@class='instructor-intro-assignment-title']")));
		assignmentpage = new AssignmentPage(driver);
		assignmentpage.editAssignmentbutton();
	    Thread.sleep(3000);
		WebElement currentdate =driver.findElement(By.xpath("//div[@class='timeZoneSpan']/span[@class='ant-calendar-picker']/span/input[@placeholder='mm/dd/yyyy']"));
		currentdate.click();
		Thread.sleep(2000);
		//WebElement todaydate = driver.findElement(By.xpath("//table[@class='ant-calendar-table']/tbody[@class='ant-calendartbody']/tr/td[@class='ant-calendar-cell ant-calendar-today']"));
		//todaydate.click();
		//assignmentpage.selectGAUDate();
		WebElement calendarInput =driver.findElement(By.xpath("//input[@class='ant-calendar-input  ']"));
		calendarInput.sendKeys("10/27/2019");
		String time2=null;
		assignmentpage.selectGAUTime(time2);
		assignmentpage.ShowAdditionalSettingsButton();
		String ShowStudentScore="Percentages";
		assignmentpage.selectshowstudentscore(ShowStudentScore);
		//WebElement Attempts = driver.findElement(By.xpath("//span[@class='ant-select-selection__rendered']"));
		assignmentpage.saveButton();
		Thread.sleep(5000);
		assignmentpage.publishButton();
		Thread.sleep(5000);
		//assignmentpage.AssignmentNameHeadTitle.click();
		String winHandleAfter = driver.getWindowHandle();
		driver.close();
		driver.switchTo().window(winHandleBefore);
		wait = new WebDriverWait(driver,8);
		TimeUnit.SECONDS.sleep(8);
		DLPpage.logoutSW5();
	}
	
	
	@Severity(SeverityLevel.NORMAL)
	@Description("CH11HW (Show Student Score In: Points")
    @Test(priority=7)
	public void CreatePremadeAssignmentCH11HW(ITestContext context) throws Exception{
		logininst = new LoginAsInstructorTest();
		logininst.loginInstructor();
		DLPpage = new LoginPage(driver);
		Thread.sleep(2000);
		DLPpage.clickSW5Icon();
		SW5DLPpage = new SW5DLPPage(driver);
		Thread.sleep(2000);
		//System.out.println(StudentID);
		String StudentID_CH11HW = (String) context.getAttribute("StudentIDAttribute");
		SW5DLPpage.selectByPartOfVisibleText(StudentID_CH11HW);
		LogUtil.log("Student ID is " + StudentID);
		Thread.sleep(3000);
		SW5DLPpage.ClickAssignmentlink("CH11HW");
		String winHandleBefore  = driver.getWindowHandle();
		for(String winHandle : driver.getWindowHandles()){
			 driver.switchTo().window(winHandle);
		}
		Thread.sleep(8000);
		driver.switchTo().frame("swfb_iframe");
		wait = new WebDriverWait(driver, 60);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//span[@class='instructor-intro-assignment-title']")));
		assignmentpage = new AssignmentPage(driver);
		assignmentpage.editAssignmentbutton();
	    Thread.sleep(3000);
		WebElement currentdate =driver.findElement(By.xpath("//div[@class='timeZoneSpan']/span[@class='ant-calendar-picker']/span/input[@placeholder='mm/dd/yyyy']"));
		currentdate.click();
		Thread.sleep(2000);
		//WebElement todaydate = driver.findElement(By.xpath("//table[@class='ant-calendar-table']/tbody[@class='ant-calendartbody']/tr/td[@class='ant-calendar-cell ant-calendar-today']"));
		//todaydate.click();
		//assignmentpage.selectGAUDate();
		WebElement calendarInput =driver.findElement(By.xpath("//input[@class='ant-calendar-input  ']"));
		calendarInput.sendKeys("10/27/2019");
		String time2=null;
		assignmentpage.selectGAUTime(time2);
		assignmentpage.ShowAdditionalSettingsButton();
		String ShowStudentScore="Points";
		assignmentpage.selectshowstudentscore(ShowStudentScore);
		//WebElement Attempts = driver.findElement(By.xpath("//span[@class='ant-select-selection__rendered']"));
		assignmentpage.saveButton();
		Thread.sleep(5000);
		assignmentpage.publishButton();
		Thread.sleep(5000);
		//assignmentpage.AssignmentNameHeadTitle.click();
		String winHandleAfter = driver.getWindowHandle();
		driver.close();
		driver.switchTo().window(winHandleBefore);
		wait = new WebDriverWait(driver,8);
		TimeUnit.SECONDS.sleep(8);
		DLPpage.logoutSW5();
	}
	@AfterTest
    public void Logout() throws InterruptedException{
		PropertiesFile.tearDownTest();
	}

}
