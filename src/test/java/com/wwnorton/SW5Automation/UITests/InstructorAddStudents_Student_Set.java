package com.wwnorton.SW5Automation.UITests;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;

import com.wwnorton.SW5Automation.objectFactory.CreateSW5_TrialAccessUser;
import com.wwnorton.SW5Automation.utilities.PropertiesFile;

public class InstructorAddStudents_Student_Set extends PropertiesFile  {
	CreateSW5_TrialAccessUser NewStudent;
	
	@Parameters("Browser")
	@BeforeTest
	public void setUp () throws Exception 
	{
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig();
		PropertiesFile.setURL();
        
    }
	
	@Severity(SeverityLevel.NORMAL)
	@Description("Create a New Trial Users")
	@Stories("Login to application and Create a New User with Trail access ")
	@Test
	public void CreateNewStudents() throws Exception{
		
		NewStudent = new CreateSW5_TrialAccessUser(driver);
		for(int i=1; i<=2; i++){
		NewStudent.Login_Smartwork5();
		
		Thread.sleep(2000);
		}
		
	}
	

}
