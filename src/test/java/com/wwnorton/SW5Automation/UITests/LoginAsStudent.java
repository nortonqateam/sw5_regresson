package com.wwnorton.SW5Automation.UITests;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;

import com.google.gson.JsonObject;
import com.wwnorton.SW5Automation.objectFactory.LoginPage;
import com.wwnorton.SW5Automation.utilities.PropertiesFile;
import com.wwnorton.SW5Automation.utilities.ReadUIJsonFile;
import com.wwnorton.SW5Automation.utilities.TestListener;



//Call to TestNG listeners to save test logs and attachments as screen shots.
@Listeners({TestListener.class })
public class LoginAsStudent extends PropertiesFile{
	
	LoginPage Smartwork5Login;
	String StudentuserName;
	String StudentPassword;
		
	// TestNG Annotations
		@Parameters("Browser")
		@BeforeTest
		
		//Test Setup
	    public void setUp () throws Exception 
		{
			PropertiesFile.readPropertiesFile();
			PropertiesFile.setBrowserConfig();
			PropertiesFile.setURL();
	        
	    }
		//Read data from Json File 
				ReadUIJsonFile readJasonObject = new ReadUIJsonFile();
				JsonObject jsonobject = readJasonObject.readUIJason();
				
			
		@Severity(SeverityLevel.NORMAL)
		@Description("Login to Smartwork5 application as a Student")
		@Stories("AS-4 Login in as Student")
		@Test
		public void LoginStudent() throws Exception {
			Smartwork5Login = new LoginPage(driver);
			StudentuserName = jsonobject.getAsJsonObject("StudentLoginCredentials").get("userName").getAsString();
			System.out.println(StudentuserName);
			StudentPassword=jsonobject.getAsJsonObject("StudentLoginCredentials").get("password").getAsString();
			Smartwork5Login.loginSW5(StudentuserName, StudentPassword);
			wait = new WebDriverWait(driver,5000L);
			wait.until(ExpectedConditions.invisibilityOf(Smartwork5Login.Overlay));
			wait.until(ExpectedConditions.visibilityOf(Smartwork5Login.gear_button_username));
			String LoginMail = Smartwork5Login.gear_button_username.getText();
			System.out.println(LoginMail);
			Assert.assertEquals(StudentuserName, LoginMail);
			Smartwork5Login.logoutSW5();
		}
		
		@AfterTest
	    public void Logout() throws InterruptedException{
			PropertiesFile.tearDownTest();
		}

			
			
			
		

}
