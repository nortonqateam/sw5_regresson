package com.wwnorton.SW5Automation.UITests;

import java.lang.reflect.InvocationTargetException;
import java.util.NoSuchElementException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.wwnorton.SW5Automation.objectFactory.CreateNewStudentSet;
import com.wwnorton.SW5Automation.objectFactory.LoginPage;
import com.wwnorton.SW5Automation.objectFactory.SW5DLPPage;
import com.wwnorton.SW5Automation.objectFactory.UpdateStudentSet;
import com.wwnorton.SW5Automation.utilities.PropertiesFile;
import com.wwnorton.SW5Automation.utilities.ReadUIJsonFile;
import com.wwnorton.SW5Automation.utilities.ReusableMethods;
import com.wwnorton.SW5Automation.utilities.TestListener;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;



//Call to TestNG listeners to save test logs and attachments as screen shots.
@Listeners({TestListener.class })



public class NoWarningMsgCopySettingsTest extends PropertiesFile {
	
	ReadUIJsonFile readJasonObject = new ReadUIJsonFile();
	JsonObject jsonObj = readJasonObject.readUIJason();

/*
	Do setup operations, get JSON response from the API and put it into JsonPath object
	Then we will do query and manipulations with JsonPath class’s methods.
*/

// TestNG Annotations
@Parameters("Browser")
@BeforeTest

//Test Setup
	public void setUp () throws Exception 
	{
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig();
		PropertiesFile.setURL();
    
	}


// Allure  Annotations

@Severity(SeverityLevel.CRITICAL)
@Description("Validate No Warning Message when Copying Settings from Source and assignment get deleted on which students have started working in Target")
@Stories("Copy Settings And Assignments")
@Test

	public void NoWarningMsgCopySettings() throws Exception 
	{
	
		String JsonTargetSS = jsonObj.getAsJsonObject("CopySettings_NoWarningMsg").get("targetStudentSetTitle").getAsString();
		String JsonSelectSourceSS = jsonObj.getAsJsonObject("CopySettings_NoWarningMsg").get("sourceStudentSetTitle").getAsString();
		String JsonSelectTargetStudentSetTitle = jsonObj.getAsJsonObject("CopySettings_NoWarningMsg").get("selectTargetStudentSetTitle").getAsString();
		
		String instructorUserName = jsonObj.getAsJsonObject("InstructorLoginCredentials").get("userName").getAsString();
		String instructorPassword = jsonObj.getAsJsonObject("InstructorLoginCredentials").get("password").getAsString();
		
		
	try {

			LoginPage login = PageFactory.initElements(driver, LoginPage.class);
			login.loginSW5(instructorUserName, instructorPassword);
		
			CreateNewStudentSet createSS = PageFactory.initElements(driver, CreateNewStudentSet.class);
			createSS.manageStudentSet();
		
			UpdateStudentSet updateSS = PageFactory.initElements(driver, UpdateStudentSet.class);
			updateSS.editStudentSet(JsonTargetSS);
			updateSS.copyAssignmentnSettings(JsonSelectSourceSS);
			Thread.sleep(5000);
			createSS.closeManageStudentSetPopUp();
			Thread.sleep(5000);
			login.clickSW5Icon();
			Thread.sleep(4000);
			SW5DLPPage dlp = PageFactory.initElements(driver, SW5DLPPage.class);
			dlp.selectSSByTitle(JsonSelectTargetStudentSetTitle);
			int count = dlp.assignmentRowCount();
			
			//int count = Integer.valueOf(rowCount.toString(); 
			
			ReusableMethods.checkPageIsReady(PropertiesFile.driver);
			
			//Validating Number of Assignments with JSON Response.
			String JSONCount = jsonObj.getAsJsonObject("CopySettings_NoWarningMsg").get("numberOfAssignment").getAsString();
			Assert.assertEquals(String.valueOf(count), JSONCount);
			
			
			//Verify that all assignment titles in UI are matching exactly with JSON - assignment titles.
			JsonObject jsonResObj = jsonObj.getAsJsonObject("CopySettings_NoWarningMsg");
			JsonArray jsonArrayObj = jsonResObj.getAsJsonArray("sourceAssignment");
			
			JsonObject jsonTitleObj = new JsonObject();
			
			WebElement AssignmentTitle;
			int matchCounter = 0;
			
			Thread.sleep(3000);
			
			try {	
				for (int i=1; i<count; i++)
				{

						AssignmentTitle = driver.findElement(By.xpath("//*[@id=\"activity_list_table\"]/tbody/tr[" + i + "]/td[1]/a"));
						boolean IsAssignmentPublished = AssignmentTitle.isDisplayed();
						
						if(IsAssignmentPublished == true) {
							
							for (int j=0; j<count; j++) 
							{
								jsonTitleObj = jsonArrayObj.get(j).getAsJsonObject();
								String jsonStrTitle = jsonTitleObj.get("title").getAsString();
					
								if(AssignmentTitle.getText().equals(jsonStrTitle))
								{
									matchCounter++;
									break;
								}
							}
					
						}
						
					}  
					
				} catch (Exception e) {
					
					throw new NoSuchElementException("Assignment is NOT published yet: " + e.getMessage());

			}
				

			//Assert.assertEquals(matchCounter, count, "All UI Assignment Titles are matching correctly?");
			
				
			//Verify that the GAU date is matching with JSON - GAU Date.
			WebElement gauDate;
			WebElement gauAssignmentTitle;
			
			try {
				
				for (int i=1; i<count; i++) 
				{	
					gauAssignmentTitle = driver.findElement(By.xpath("//*[@id=\"activity_list_table\"]/tbody/tr[" + i + "]/td[1]/a"));
					boolean IsAssignmentPublished = gauAssignmentTitle.isDisplayed();
					
					if (IsAssignmentPublished == true)
					{
						gauDate = driver.findElement(By.xpath("//*[@id=\"activity_list_table\"]/tbody/tr[" + i + "]/td[3]"));
						String strGAUDate = gauDate.getText(); 
						String strAssignmentTitle = gauAssignmentTitle.getText();

						for (int j=0; j<count; j++)
						{	
					
							jsonTitleObj = jsonArrayObj.get(j).getAsJsonObject();
							String jsonGAUDate = jsonTitleObj.get("gau").getAsString();
							String jsonStrTitle = jsonTitleObj.get("title").getAsString();
					
							if(!strGAUDate.equals("—") && strAssignmentTitle.equalsIgnoreCase(jsonStrTitle))
							{
								Assert.assertEquals(strGAUDate, jsonGAUDate , "Assignment with GAU Date: " + " " + jsonStrTitle);
								System.out.println("Assignment with GAU in UI matches with JSON - Assignment with GAU: " + gauAssignmentTitle.getText());
								break;
							}
						
						}
					}
		
				}
			} catch (Exception e) {
				
				throw new NoSuchElementException("Assignment is NOT published yet: " + e.getMessage()); 
				
		}
			 
			 
			//Verify that the Adaptive assignment in UI is matching with JSON - Adaptive value.
				WebElement adptiveTitle;
				
				try {
					for (int i=1; i<count; i++) 
					{
						adptiveTitle = driver.findElement(By.xpath("//*[@id=\"activity_list_table\"]/tbody/tr[" + i + "]/td[1]/a"));
						boolean IsAssignmentPublished = adptiveTitle.isDisplayed();
						boolean isAdaptive = ReusableMethods.elementExist(PropertiesFile.driver, "//*[@id=\"activity_list_table\"]/tbody/tr[" + i + "]/td[1]/img");
						
						if(IsAssignmentPublished == true)
						{
							for (int j=0; j<count; j++)
							{	
								jsonTitleObj = jsonArrayObj.get(j).getAsJsonObject();
								String jsonAdaptiveStrValue = jsonTitleObj.get("isAdaptive").getAsString();
								String jsonStrTitle = jsonTitleObj.get("title").getAsString();
					 
								if(isAdaptive && adptiveTitle.getText().equals(jsonStrTitle))
									{		
										Assert.assertEquals(jsonAdaptiveStrValue, "true");
										System.out.println("Adaptive Assignment in UI matches with JSON - Adaptive: " + adptiveTitle.getText());	
						
									}
					
								else if(!isAdaptive && adptiveTitle.getText().equals(jsonStrTitle)) 
									{
						
										Assert.assertEquals(jsonAdaptiveStrValue, "false");
							
									}
								}
						
							}
		
						}
					} catch (Exception e) {
				
				throw new NoSuchElementException("Assignment is NOT published yet: " + e.getMessage()); 
				
		}
				
			 
			login.logoutSW5();

		}

        catch (InvocationTargetException ex) {
                	System.err.println("An InvocationTargetException was caught!");
                	Throwable cause = ex.getCause();
                	System.out.format("Invocation of %s failed because of: %s%n", cause.getMessage());
            	}

    	catch (ClassNotFoundException | InstantiationException | IllegalAccessException ex) {
    			System.err.println("The following exception was thrown:");
    			ex.printStackTrace();
    		}
	
	}


	// Closing driver and test.

	@AfterTest	
	public void closeTest() throws Exception {
	
	PropertiesFile.tearDownTest();

	}

}
