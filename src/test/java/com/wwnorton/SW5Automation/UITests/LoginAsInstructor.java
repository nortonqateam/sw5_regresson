package com.wwnorton.SW5Automation.UITests;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;

import com.google.gson.JsonObject;
import com.wwnorton.SW5Automation.objectFactory.LoginPage;
import com.wwnorton.SW5Automation.utilities.PropertiesFile;
import com.wwnorton.SW5Automation.utilities.ReadUIJsonFile;
import com.wwnorton.SW5Automation.utilities.TestListener;


@Listeners({TestListener.class })
public class LoginAsInstructor extends PropertiesFile {
	
	LoginPage Smartwork5Login;
	String userName;
	String Password;
	@Parameters("Browser")
	@BeforeTest
	public void setUp () throws Exception 
	{
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig();
		PropertiesFile.setURL();
        
    }
	//Read data from Json File 
	ReadUIJsonFile readJasonObject = new ReadUIJsonFile();
	JsonObject jsonobject = readJasonObject.readUIJason();
 
	// Allure  Annotations

		@Severity(SeverityLevel.NORMAL)
		@Description("Login as a Instructor")
		@Stories("Login to application as a Instructor ")
		@Test(priority=0)
		public void loginInstructor() throws Exception{
			Smartwork5Login = new LoginPage(driver);
			userName = jsonobject.getAsJsonObject("InstructorLoginCredentials").get("userName").getAsString();
			Password=jsonobject.getAsJsonObject("InstructorLoginCredentials").get("password").getAsString();
			Smartwork5Login.loginSW5(userName, Password);
			wait = new WebDriverWait(driver,5000L);
			wait.until(ExpectedConditions.invisibilityOf(Smartwork5Login.Overlay));
			wait.until(ExpectedConditions.visibilityOf(Smartwork5Login.gear_button_username));
			String LoginMail = Smartwork5Login.gear_button_username.getText().toLowerCase();
			Smartwork5Login.verifySignin(userName, LoginMail);
			//Assert.assertEquals(userName, LoginMail, "The UserName is displayed as expected");
			//Assert.assertEquals(Smartwork5Login.verifySignin(userName), LoginMail);
			// Assert.assertTrue(Smartwork5Login.verifySignin(userName, LoginMail), "The User details is displayed as expected and Matches");
			Thread.sleep(2000);
			
			
		}
		@Test(priority=1)
		public void logoutInstructor() throws Exception{
			Smartwork5Login.logoutSW5();
		}
		
		
	
		@AfterTest
	    public void Logout() throws InterruptedException{
			PropertiesFile.tearDownTest();
		}

}
