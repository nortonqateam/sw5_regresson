/**
 * 
 */
package com.wwnorton.SW5Automation.UITests;

import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;

import com.google.gson.JsonObject;
import com.wwnorton.SW5Automation.objectFactory.AssignmentPage;
import com.wwnorton.SW5Automation.objectFactory.Create_New_Student_Set_Page;
import com.wwnorton.SW5Automation.objectFactory.LoginPage;
import com.wwnorton.SW5Automation.objectFactory.ManageStudentSetsPage;
import com.wwnorton.SW5Automation.objectFactory.Questions_Page;
import com.wwnorton.SW5Automation.objectFactory.SW5DLPPage;
import com.wwnorton.SW5Automation.utilities.PropertiesFile;
import com.wwnorton.SW5Automation.utilities.ReadUIJsonFile;

/**
 * @author Shripad
 *
 * 07-Oct-2019
 */
public class InstructorCreateNonAdaptiveAssignmentfromscratch extends PropertiesFile{
	LoginAsInstructor logininst;
	LoginPage DLPpage;
	Create_New_Student_Set_Page createStudentset;
	ManageStudentSetsPage managestudentsetpage;
	SW5DLPPage SW5DLPpage;
	AssignmentPage assignmentpage;
	Questions_Page questionspage;
	WebDriverWait wait;
	
	ReadUIJsonFile readJasonObject = new ReadUIJsonFile();
	JsonObject jsonObj = readJasonObject.readUIJason();	
	
	
	@Parameters("Browser")
	@BeforeTest
	public void setUp () throws Exception 
	{
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig();
		PropertiesFile.setURL();
        
    }
	
	@Severity(SeverityLevel.NORMAL)
	@Description("As an instructor -  Create Non Adaptive Assignment from scratch with list of assignment questions and policy.")
	@Stories("AS-12 As an instructor -  Create Non Adaptive Assignment from scratch with list of assignment questions and policy. ")
	@Test
	public void CreateNonAdaptiveAssignment() throws Exception{
		logininst = new LoginAsInstructor();
		logininst.loginInstructor();
		createStudentset = new Create_New_Student_Set_Page(driver);
		createStudentset.SignIn_Register();
		createStudentset.ManageStudentSetlink();
		createStudentset.CreateStudentSetButton();
		createStudentset.createNewStudentset();
		createStudentset.createStudentset_information();
		String StudentID  = createStudentset.createStudentset_ID();
		System.out.println(StudentID);
		managestudentsetpage = new ManageStudentSetsPage(driver);
		managestudentsetpage.clickcloselink();
		DLPpage = new LoginPage(driver);
		Thread.sleep(2000);
		DLPpage.clickSW5Icon();
		Thread.sleep(2000);
		SW5DLPpage = new SW5DLPPage(driver);
		SW5DLPpage.selectByPartOfVisibleText(StudentID);
		Thread.sleep(8000);
		SW5DLPpage.ClickAssignment();
		/*String ChildWindow = null;
	    Set<String> Windowhandles= driver.getWindowHandles();
	    String ParentWindow = driver.getWindowHandle();
	    Windowhandles.remove(ParentWindow);
	    String winHandle=Windowhandles.iterator().next();
	    if (winHandle!=ParentWindow){
	    	ChildWindow=winHandle;
	    }
	    driver.switchTo().window(ChildWindow);*/
		
		
		
		
		
		
		String winHandleBefore  = driver.getWindowHandle();
		for(String winHandle : driver.getWindowHandles()){
			 driver.switchTo().window(winHandle);
		}
		
		Thread.sleep(8000);
		driver.switchTo().frame("swfb_iframe");
		wait = new WebDriverWait(driver, 60);
		 
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//span[@class='instructor-intro-assignment-title']")));
		assignmentpage = new AssignmentPage(driver);
		assignmentpage.editAssignmentbutton();
		String Assignmentname = jsonObj.getAsJsonObject("NonAdaptiveInfo").get("NonAdaptiveAssignemntName").getAsString();
		assignmentpage.enterAssignmentName(Assignmentname);
		assignmentpage.gauDate();
		Thread.sleep(5000);
		assignmentpage.adaptiveOff();
		assignmentpage.ShowAdditionalSettingsButton();
		
		questionspage = new Questions_Page(driver);
		questionspage.addQuestions();
		
		wait = new WebDriverWait(driver,20);
		TimeUnit.SECONDS.sleep(20);
		wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//div[@class='head-box']"))));
		questionspage.questionsLibraryList_add();
		Thread.sleep(5000);
		questionspage.headTitleLink();
		wait = new WebDriverWait(driver,8);
		TimeUnit.SECONDS.sleep(8);
		wait.until(ExpectedConditions.visibilityOf(assignmentpage.SaveButton));
		assignmentpage.saveButton();
		Thread.sleep(5000);
		assignmentpage.publishButton();
		Thread.sleep(5000);
		questionspage.headTitleLink();
		String winHandleAfter = driver.getWindowHandle();
		driver.close();
		driver.switchTo().window(winHandleBefore);
		/*wait = new WebDriverWait(driver,8);
		TimeUnit.SECONDS.sleep(8);
		assignmentpage.Assignmentlinks(Assignmentname);*/
		//Wait Command
		//driver.close();
	}
	@AfterTest	
	public void closeTest() throws Exception {
		
		PropertiesFile.tearDownTest();
	
	}
	
}
