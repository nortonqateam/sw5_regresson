package com.wwnorton.SW5Automation.utilities;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.jayway.restassured.RestAssured;

import ru.yandex.qatools.allure.annotations.Step;



public class PropertiesFile {
	
	public static WebDriver driver = null;
    public static WebDriverWait wait;
    public static String UserDir=System.getProperty("user.dir");
    public static String Browser;
	public static String url;
	public static String DriverPath;
	
	public static String baseURI;
	public static String serverPort;
	public static String basePathTerm;
	
	
    public WebDriver getDriver() {
    	
        return driver;
    }
	
    
    // Read Browser name, Test url and Driver file path from config.properties.
    
	public static void readPropertiesFile() throws Exception {
		
		Properties prop = new Properties();
		
		try {
			
			InputStream input = new FileInputStream(UserDir +"\\src\\test\\resources\\config.properties");
			prop.load(input);
			
			Browser = prop.getProperty("browsername");
			url = prop.getProperty("testurl");
			//DriverPath = prop.getProperty("driverfile");
			baseURI = prop.getProperty("uri");
			serverPort = prop.getProperty("port");
			basePathTerm = prop.getProperty("path");
			DriverPath =UserDir +"\\Drivers\\";
		} catch (Exception e) {
			
			e.printStackTrace();
		}

		
	}	

	
	// Set Browser configurations by comparing Browser name and Diver file path.
	
	public static void setBrowserConfig() {
		
			if(Browser.contains("Firefox")) {
				System.setProperty("webdriver.gecko.driver", DriverPath + "/geckodriver.exe");
            
				FirefoxOptions firefoxOptions = new FirefoxOptions();
				firefoxOptions.setHeadless(true);
				firefoxOptions.setCapability("marionette", true);
            
				driver = new FirefoxDriver(firefoxOptions); 
            
			}
			
			if(Browser.contains("Chrome")) {
				System.setProperty("webdriver.chrome.driver", DriverPath + "/chromedriver.exe");
			
				DesiredCapabilities desiredCapabilities = DesiredCapabilities.chrome();
				ChromeOptions chromeOptions = new ChromeOptions();
				//chromeOptions.setHeadless(true);
				chromeOptions.addArguments("--test-type");
				chromeOptions.addArguments("--start-maximized");
				chromeOptions.addArguments("--disable-infobars");
				chromeOptions.addArguments("--disable-extensions");
				chromeOptions.addArguments("--disable-gpu");
				chromeOptions.addArguments("--no-default-browser-check");
				chromeOptions.addArguments("--ignore-certificate-errors");
				chromeOptions.addArguments("--proxy-server='direct://'");
				chromeOptions.addArguments("--proxy-bypass-list=*");
				chromeOptions.setExperimentalOption("useAutomationExtension", false);
			    //chromeOptions.addArguments("--headless");
			   // chromeOptions.addArguments("--window-size=1920,1080");
				desiredCapabilities.setCapability(ChromeOptions.CAPABILITY, chromeOptions);
				driver = new ChromeDriver(chromeOptions);

           }
			
		}
		

	// Set Test URL based on config.properties file.
	
	public static void setURL() {
		
			driver.manage().window().maximize();
			driver.get(url);
		
		}
		
	   /*
    Sets Base URI
    Before starting the test, we should set the RestAssured.baseURI
    */
    
	@Step("Set Base URI,  Method: {method} ")
    public static void setBaseURI (){
        RestAssured.baseURI = baseURI;
    }

    /*
    Sets Base path
    Before starting the test, we should set the RestAssured.basePath
    */
	
	@Step("Set Base Port,  Method: {method} ")
    public static void setServerPort(){
        RestAssured.port = Integer.valueOf(serverPort);
    }
    
    /*
    Sets Base path
    Before starting the test, we should set the RestAssured.basePath
    */
	
	@Step("Set Base Path,  Method: {method} ")
    public static void setBasePath(String basepath){
        RestAssured.basePath = basepath;
    }

    /*
    Reset Base URI (after test)
    After the test, we should reset the RestAssured.basePath
    */
    
    public static void resetBaseURI (){
        RestAssured.baseURI = null;
    }

    /*
    Reset Server Port (after test)
    After the test, we should reset the RestAssured.serverPort
    */
    
    @SuppressWarnings("null")
	public static void resetServerPort (){
        RestAssured.port = (Integer) null;
    }
    
    /*
    Reset base path (after test)
    After the test, we should reset the RestAssured.basePath
    */
    
    public static void resetBasePath(){
        RestAssured.basePath = null;
    }
	
	// Close the driver after running test script.
	
	public static void tearDownTest() throws InterruptedException {
		
			wait = new WebDriverWait(driver,3);
			driver.close();
			//driver.quit();
					
		}


}
