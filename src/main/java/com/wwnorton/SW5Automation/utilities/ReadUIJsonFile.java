package com.wwnorton.SW5Automation.utilities;

	import java.io.FileReader;

	import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.stream.JsonReader;



	public class ReadUIJsonFile {
		
		public static String UserDir=System.getProperty("user.dir");
		// Read Json test data from testData.json file with the parameters and values.
		
		public JsonObject readUIJason() {
			
				JsonObject rootObject = null;
			
			try
			{
			
				JsonParser parser = new JsonParser();
				JsonReader jReader = new JsonReader(new FileReader(UserDir +"//src//test//resources//uiData.json"));
				jReader.setLenient(true);
				
				JsonElement rootElement = parser.parse(jReader);
				rootObject = rootElement.getAsJsonObject();
			
			}
			
			catch(Exception e)
			
			{
				e.printStackTrace();
			}
			
			return rootObject;
			
		}
			
	}

