package com.wwnorton.SW5Automation.utilities;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class GetDate {

	static Date currentDate;
	private final static DateFormat dateFormat = new SimpleDateFormat(
			"MM/dd/yyyy");
	Calendar c = Calendar.getInstance();

	/*public static void main(String[] args){
		GetDate d = new GetDate();
		
	}*/
	 

	public static String getCurrentDate() {

		currentDate = new Date();
		String SystemDate = (dateFormat.format(currentDate));
		return SystemDate;
	}

	public String getNextmonthDate() {
		// convert date to calendar

		c.setTime(currentDate);

		// manipulate date
		// c.add(Calendar.YEAR, 1);
		c.add(Calendar.MONTH, 1);
		c.add(Calendar.DATE, 1); // same with c.add(Calendar.DAY_OF_MONTH, 1);

		// convert calendar to date
		Date currentDatePlusOne = c.getTime();

		String nextmonthdate = (dateFormat.format(currentDatePlusOne));
		return nextmonthdate;

	}

	public String addDaystoCalendar(String addedDays) {
		// convert date to calendar
		getCurrentDate();
		c.setTime(currentDate);

		// manipulate date
		// c.add(Calendar.YEAR, 1);
		// c.add(Calendar.MONTH, 1);
		c.add(Calendar.DATE, Calendar.DATE + 10); // same with
													// c.add(Calendar.DAY_OF_MONTH,
													// 1);

		// convert calendar to date
		Date currentDatePlusTendays = c.getTime();

		addedDays = (dateFormat.format(currentDatePlusTendays));
		System.out.println(addedDays);
		return addedDays;

	}

	

}
