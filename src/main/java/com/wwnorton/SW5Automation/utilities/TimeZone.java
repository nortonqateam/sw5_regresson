/**
 * 
 */
package com.wwnorton.SW5Automation.utilities;

/**
 * @author Shripad
 *
 * 15-Oct-2019
 */

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;

public class TimeZone {
	private static final String DATE_FORMAT = "hh:mm:ss a z";
	private static final DateTimeFormatter formatter = DateTimeFormatter
			.ofPattern(DATE_FORMAT);
	/*public static void main(String[] args){
		System.out.println(getTime());

	}*/
	public static String getTime() {
		ZoneId fromTimeZone = ZoneId.of("Asia/Kolkata"); // Source timezone
		ZoneId toTimeZone = ZoneId.of("America/New_York"); // Target timezone

		LocalDateTime today = LocalDateTime.now(); // Current time

		// Zoned date time at source timezone
		ZonedDateTime currentISTime = today.atZone(fromTimeZone);

		// Zoned date time at target timezone
		ZonedDateTime currentETime = currentISTime
				.withZoneSameInstant(toTimeZone);
		
		LocalTime minutes = currentETime.toLocalTime().plusMinutes(60);
		/*if(minutes.isBefore(LocalTime.of(06, 00)) && (minutes.isAfter(LocalTime.of(12,00)))){
			LocalTime time =minutes.now();
		*/
		
		String RoundoffTime =getNearestHourQuarter(minutes);
		
		
		return RoundoffTime;

	}

	public static String getNearestHourQuarter(LocalTime dt2) {
		int hour = dt2.getHour();
		int minutes = dt2.getMinute();
		
		String meridiam1 = dt2.format(DateTimeFormatter.ofPattern("a"));

		if (minutes > 30) {
			hour = hour + 1;
			minutes = 0;
		} else {
			minutes = 30;
		}
		return  hour%12 + ":"
				+ String.format("%02d", minutes) + " " + meridiam1;
	}
	
	
	
}
