/**
 * 
 */
package com.wwnorton.SW5Automation.utilities;

import java.util.Set;

import org.openqa.selenium.WebDriver;

/**
 * @author Shripad
 *
 * 26-Sep-2019
 */
public class GetWindowHandle {
	
	WebDriver driver;
	
	public void navigatetoChildwindow(){
	String ChildWindow = null;
    Set<String> Windowhandles= driver.getWindowHandles();
    String ParentWindow = driver.getWindowHandle();
    Windowhandles.remove(ParentWindow);
    String winHandle=Windowhandles.iterator().next();
    if (winHandle!=ParentWindow){
    	ChildWindow=winHandle;
    }
    driver.switchTo().window(ChildWindow);
	}
}
