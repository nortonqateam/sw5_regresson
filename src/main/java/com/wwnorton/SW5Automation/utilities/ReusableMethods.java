package com.wwnorton.SW5Automation.utilities;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;

import ru.yandex.qatools.allure.annotations.Attachment;


public class ReusableMethods {
	
	public static void checkPageIsReady(WebDriver driver) {
		 
		boolean isPageReady = false;
		JavascriptExecutor js = (JavascriptExecutor) driver;
		//int i=1;
		 
		while (!isPageReady) {
			
			isPageReady = js.executeScript("return document.readyState").toString().equals("complete");
			//System.out.println("i=" + i++ + "isPageReady=" + isPageReady);
			
			try {
				
					Thread.sleep(1000);
			
			} catch (InterruptedException e) {
				
				e.printStackTrace();
			
			}
			
		}
	}
	
	public static boolean elementExist (WebDriver driver, String xpath) {
	    
		try {
	        
			driver.findElement(By.xpath(xpath));
			
	    } catch (NoSuchElementException e) 
		
		{
	        return false;
	    }
	    
		return true;
	}
	
	

}