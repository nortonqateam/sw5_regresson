package com.wwnorton.SW5Automation.APIUtils;


import com.jayway.restassured.http.ContentType;
import com.jayway.restassured.path.json.JsonPath;
import com.jayway.restassured.response.Response;

import ru.yandex.qatools.allure.annotations.Step;

import static com.jayway.restassured.RestAssured.get;
import static com.jayway.restassured.RestAssured.given;



public class ApiUtils {
    
	//Global Setup Variables
	
    public static String path;
    public static String jsonPathTerm;


    /*
	Sets ContentType
    We should set content type as JSON or XML before starting the test
    */
    
    public static void setContentType (ContentType Type){
        given().contentType(Type);
    }

    
    /*
    Returns response
    We send "path" as a parameter to the Rest Assured'a "get" method
    and "get" method returns response of API
    */
    
    public static Response getResponsebyPath(String path) {
        return get(path);
    }
    
    // Returns response
    
    public static Response getResponse() {
        return get();
    }

    /*
    Returns JsonPath object
    * First convert the API's response to String type with "asString()" method.
    * Then, send this String formatted json response to the JsonPath class and return the JsonPath
    */
    
	@Step("Get JSON Path for the API Response,  Method: {method} ")
    public static JsonPath getJsonPath (Response res) {
        
    	String json = res.asString();
        return new JsonPath(json);
    }
}
