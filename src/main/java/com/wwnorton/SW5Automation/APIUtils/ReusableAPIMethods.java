package com.wwnorton.SW5Automation.APIUtils;

import com.jayway.restassured.path.json.JsonPath;
import com.jayway.restassured.response.Response;

import ru.yandex.qatools.allure.annotations.Step;

import java.util.*;

import org.testng.Assert;

 
public class ReusableAPIMethods {
    /*
    Verify the http response status returned. Check Status Code is 200?
    We can use Rest Assured library's response's getStatusCode method
    */
    
	@Step("Validate Status code and Status message,  Method: {method} ")
    public static void checkStatusIs200 (Response res) {
    	
    	int statusCode = res.getStatusCode();
        Assert.assertEquals(statusCode, 200, "Status Check Passed?");
        
        String successCode = res.jsonPath().get("statusMessage");
        Assert.assertEquals( successCode, "Success", "OPERATION_SUCCESS?");
        
    }
    
    //Get Clients
    public ArrayList getClients (JsonPath jp) {
        ArrayList clientList = jp.get();
        return clientList;
    }
    
}

