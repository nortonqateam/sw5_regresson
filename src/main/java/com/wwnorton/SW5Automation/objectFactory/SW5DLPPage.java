package com.wwnorton.SW5Automation.objectFactory;

import java.util.List;
import java.util.concurrent.TimeUnit;

import junit.framework.Assert;

import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.wwnorton.SW5Automation.utilities.LogUtil;
import com.wwnorton.SW5Automation.utilities.PropertiesFile;
import com.wwnorton.SW5Automation.utilities.ReusableMethods;

import ru.yandex.qatools.allure.annotations.Step;

public class SW5DLPPage {

WebDriver driver;
String AssignmentText;
JavascriptExecutor js = (JavascriptExecutor) driver;
	// Finding Web Elements on SW5 DLP page using PageFactory.
	
	@FindBy(how = How.XPATH, using = "//*[@id=\"report_class_menu\"]")
	public WebElement StudentSet;
	
	@FindBy(how = How.XPATH, using = "//*[@id=\"activity_list_table\"]/tbody")
	public WebElement AssignmentTitle;
	
	@FindBy(how = How.ID, using="create_custom_swfb_assignment_button")
	public WebElement Create_New_Assignment;
	
	// Initializing Web Driver and PageFactory.
	
		public SW5DLPPage(WebDriver driver){
			
			this.driver = driver;
			PageFactory.initElements(driver, this);
		
		}
		
		
		@Step("Select New Student Set ID by Visible Text on SW5 DLP page,  Method: {method} ")
		public void selectSSByTitle(String SSTitle) throws Exception {
			
			WebDriverWait wait = new WebDriverWait(driver,10);
			
			wait.until(ExpectedConditions.elementToBeClickable(StudentSet));
			Select drpStudentSet = new Select(StudentSet);
			drpStudentSet.selectByVisibleText(SSTitle);
			
		}
		
		
		@Step("Select New Student Set ID by Value on SW5 DLP page,  Method: {method} ")
		public void selectSSByValue(String SSTitleValue) throws Exception {
			
			WebDriverWait wait = new WebDriverWait(driver,10);
			
			wait.until(ExpectedConditions.elementToBeClickable(StudentSet));
			Select drpStudentSet = new Select(StudentSet);
			drpStudentSet.selectByValue(SSTitleValue);
			
		}
		
		
		@Step("Select New Student Set ID on SW5 DLP page,  Method: {method} ")
		public int assignmentRowCount() throws Exception {
			
			//WebDriverWait wait = new WebDriverWait(driver,10);
			//wait.until(ExpectedConditions.visibilityOfAllElements(AssignmentTitle));
			//List<WebElement> assignmentRows = AssignmentTitle.findElements(By.tagName("tr"));
			//int rowsCount = assignmentRows.size();
			
			Thread.sleep(5000);
			JavascriptExecutor js = (JavascriptExecutor) driver;
			Object rowCountObj = js.executeScript("return $('#activity_list_table tbody tr').length");
			
			return Integer.valueOf(rowCountObj.toString());
		
		}
		
		
		@Step("Select New Student Set ID by option Value on SW5 DLP page,  Method: {method} ")
		public void selectByPartOfVisibleText(String value) {
		    List<WebElement> optionElements = StudentSet.findElements(By.tagName("option"));

		    for (WebElement optionElement: optionElements) {
		        if (optionElement.getText().contains(value)) {
		            System.out.println(optionElement.getText());
		            optionElement.click();
		            break;
		        }
		    }

		    
		
		}
		
		@Step("Select New Student Set ID by option Value on SW5 DLP page,  Method: {method} ")
		public void ClickAssignment() {
			Create_New_Assignment.click();
		}

		@Step("Select Assignment from Assignment Title Column,  Method: {method} ")
		public void ClickAssignmentlink(String AssignmentTitle){
			List<WebElement> links = driver.findElements(By.xpath("//div[@id='activity_list_table_wrapper']/table/tbody"));
			int linkCount = links.size();
			for(int i=0;i<links.size();i++){
	            List<WebElement> linkstext=links.get(i).findElements(By.xpath("//tr/td[@class=' title_td']/a"));
	            for(int j=0; j<linkstext.size(); j++){
	            	AssignmentText = linkstext.get(j).getText();
	            	if(AssignmentText.equalsIgnoreCase(AssignmentTitle)){
	            		LogUtil.log("The Assignment Name " + AssignmentTitle );
	            	    //Assert.assertSame("The AssignmentName is" , "CH1H4W", AssignmentTitle);
	            		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();",linkstext.get(j));
	            		((JavascriptExecutor) driver).executeScript("window.scrollBy(0,-100)");
	                	linkstext.get(j).click();
	                }
	            }
	            
	            
	        }

		}
		
		@Step("Select Assignment from Assignment Title Column,  Method: {method} ")
		public void ClickAssignmentlinkbylinkText(String AssignmentTitle){
			WebElement AssignmentNameclick =driver.findElement(By.linkText(AssignmentTitle));
			AssignmentNameclick.click();
		}
	
		
		private void scrollToElement(WebElement el) {
		    if (driver instanceof JavascriptExecutor) {
		        ((JavascriptExecutor) driver)
		            .executeScript("arguments[0].scrollIntoView(true);", el);
		    }
		}
		
		public void waitAndClick(WebElement el) {
			try {
				WebDriverWait wait = new WebDriverWait(driver, 10, 200);
				wait.until(ExpectedConditions.invisibilityOfElementLocated(By.linkText("CH9HW")));
				wait.until(ExpectedConditions.elementToBeClickable(el)).click();
				
			} catch (WebDriverException  e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				scrollToElement(el);
				el.click();
			}
		}
		
}

