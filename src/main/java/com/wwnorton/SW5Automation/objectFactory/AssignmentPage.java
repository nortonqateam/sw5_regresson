/**
 * 
 */
package com.wwnorton.SW5Automation.objectFactory;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.wwnorton.SW5Automation.utilities.LogUtil;
import com.wwnorton.SW5Automation.utilities.TimeZone;

import ru.yandex.qatools.allure.annotations.Step;

/**
 * @author Shripad
 *
 *         27-Sep-2019
 */
public class AssignmentPage {
	com.wwnorton.SW5Automation.utilities.GetDate getsystemdate = new com.wwnorton.SW5Automation.utilities.GetDate();
	WebDriver driver;
	WebDriverWait wait;
	JavascriptExecutor js = (JavascriptExecutor) driver;
	TimeZone timetoselect = new TimeZone();

	@FindBy(how = How.XPATH, using = "//div[@class='container-box one-rower-box bgFFF']/div/button/span[contains(text(),'EDIT ASSIGNMENT')]")
	public WebElement EditAssignmentbutton;

	@FindBy(how = How.XPATH, using = "//div[@type='text']/input[@placeholder ='Your assignment name']")
	public WebElement AssignmentName;

	@FindBy(how = How.XPATH, using = "//input[@class='ant-calendar-picker-input ant-input']")
	public WebElement GAUDate;

	@FindBy(how = How.XPATH, using = "//table[@class='ant-calendar-table']/tbody[@class='ant-calendartbody']/tr/td/span[@class='ant-calendar-date']")
	public WebElement calendarGAUDate;

	@FindBy(how = How.XPATH, using = "//div[@class='general-settings']/span[@class='ant-switch']")
	public WebElement AdaptiveOff;

	@FindBy(how = How.XPATH, using = "//div[@class='general-settings']/span[@class='ant-switch ant-switch-checked']")
	public WebElement AdaptiveON;

	@FindBy(how = How.ID, using = "warm-up")
	public WebElement Warmup;

	@FindBy(how = How.NAME, using = "follow-up")
	public WebElement followup;

	@FindBy(how = How.XPATH, using = "//button[@class='btn-submit-s btn-info lato-regular-14']/span[contains(text(),'SELECT OBJECTIVES')]")
	public WebElement SELECTOBJECTIVES;

	@FindBy(how = How.XPATH, using = "//div[@class='modal-body']/div[@class='regular-modal-body']")
	public WebElement LearningObjectives_popup;

	@FindBy(how = How.XPATH, using = "//input[@class='ant-checkbox-input']")
	public WebElement LearningObjectives_checkbox;

	@FindBy(how = How.XPATH, using = "//button[@class='btn-submit-s btn-primary lato-regular-14 fr'][contains(.,'DONE')]")
	public WebElement LearningObjectives_DoneButton;

	@FindBy(how = How.XPATH, using = "//button[@class='btn-submit-s btn-primary lato-regular-14 fr'][contains(.,'CANCEL')]")
	public WebElement LearningObjectives_CancelButton;

	@FindBy(how = How.XPATH, using = "//div[@class='form-item-icon-title']/b[contains(.,'Learning Objectives:')]/following-sibling::span")
	public WebElement objectiveselected_info;

	@FindBy(how = How.XPATH, using = "//span[@class='saveBtnName']")
	public WebElement SaveButton;

	@FindBy(how = How.XPATH, using = "//button[@class='btn-submit-s btn-primary lato-regular-14']/span[contains(text(),'PUBLISH')]")
	public WebElement PublishButton;

	@FindBy(how = How.CSS, using = ".ant-calendar-picker-input")
	public WebElement DatePicker;

	@FindBy(how = How.XPATH, using = "//div[@class='assignment-switch']/span[contains(text(),'SHOW ADDITIONAL SETTINGS')]")
	public WebElement ShowAdditionalSettingsButton;

	@FindBy(how = How.XPATH, using = "//div[@type='select']/select[@class='body-content form-control']")
	public WebElement SelectTime;

	@FindBy(how = How.XPATH, using = "//select[@data-reactid='.0.1.7.0.0.1.0.1.0.1.1:$input']")
	public WebElement SelectUngradedPractice;

	@FindBy(how = How.XPATH, using = "//select[@data-reactid='.0.1.7.0.0.1.0.0.1.1.1:$input']")
	public WebElement SelectShowsolution;

	@FindBy(how = How.XPATH, using = "//span[@class='ant-select-selection__rendered']")
	public WebElement SelectAttempts;

	@FindBy(how = How.XPATH, using = "//button[@class='btn-submit-xs btn-info clear-margin']/span[contains(text(),'APPLY TO ALL')]")
	public WebElement ApplyToAllButton;

	@FindBy(how = How.XPATH, using = "//select[@data-reactid='.0.1.7.0.0.1.0.1.3.1.1:$input']")
	public WebElement SelectShowstudentscore;

	public AssignmentPage(WebDriver driver) {

		this.driver = driver;
		PageFactory.initElements(driver, this);

	}

	@Step("User Click the Edit Assignment button,  Method: {method} ")
	public void editAssignmentbutton() {
		System.out.println("The Assignement Begins ");
		EditAssignmentbutton.click();
		System.out.println("The Assignement Clicked ");
	}

	@Step("User enters the Assignment Name,  Method: {method} ")
	public String enterAssignmentName(String assignmentName) {
		AssignmentName.sendKeys(assignmentName);
		return assignmentName;
	}

	@Step("User selects the GAUDate,  Method: {method} ")
	public void gauDate() throws InterruptedException {
		// GAUDate.click();

		driver.findElement(By.cssSelector(".ant-calendar-picker-input"))
				.click();
		Thread.sleep(2000);
		List<WebElement> datelist = driver.findElements(By
				.xpath("//table[@class='ant-calendar-table']/tbody/tr/td"));
		for (WebElement e : datelist) {
			String date = e.getText();
			int dateInt = Integer.parseInt(date);
			if (dateInt == (25)) {
				e.click();
				break;
			}
		}

	}

	@Step("User selects the GAUDate,  Method: {method} ")
	public String gauCurrentDate() throws InterruptedException {
		// GAUDate.click();

		/*
		 * driver.findElement(By.cssSelector(".ant-calendar-picker-input")).click
		 * (); Thread.sleep(2000); List<WebElement> datelist
		 * =driver.findElements
		 * (By.xpath("//table[@class='ant-calendar-table']/tbody/tr/td"));
		 * for(WebElement e :datelist ){ String date = e.getText(); int dateInt
		 * =Integer.parseInt(date); System.out.println(dateInt); int currentDate
		 * = Integer.parseInt(GetDate.getCurrentDate());
		 * System.out.println(currentDate); if(dateInt==(currentDate)){
		 * e.click(); break; } }
		 */
		SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
		String CurrentDate = dateFormat.format(new Date());

		return CurrentDate;
	}

	@Step("User select the ON option for Adaptive Option,  Method: {method} ")
	public void adaptiveOn() {
		boolean adaptiveOn = false;
		if (adaptiveOn == true) {
			AdaptiveON.click();
		}
	}

	@Step("User select the OFF option for Adaptive Option,  Method: {method} ")
	public void adaptiveOff() {

		WebElement oSwitchbuttons = driver.findElement(By
				.xpath("//div[@class='general-settings']/span/span"));
		String OnOffText = oSwitchbuttons.getText();
		if (OnOffText.equalsIgnoreCase("ON")) {
			oSwitchbuttons.click();
		}

	}

	@Step("User select the Warm-Up option ,  Method: {method} ")
	public void warmUp() {
		/*
		 * boolean warmUp = true; if(warmUp==false)
		 */
		Warmup.click();

	}

	@Step("Click Learnings Objective Button and select the Learnings Objectives ,  Method: {method} ")
	public void selectObejctive() throws InterruptedException {
		SELECTOBJECTIVES.click();
		wait = new WebDriverWait(driver, 8);
		TimeUnit.SECONDS.sleep(8);
		wait.until(ExpectedConditions.visibilityOf(LearningObjectives_popup));
		LearningObjectives_checkbox.click();
		LearningObjectives_DoneButton.click();
		wait = new WebDriverWait(driver, 8);
		TimeUnit.SECONDS.sleep(8);
		wait.until(ExpectedConditions.visibilityOf(objectiveselected_info));
		String text = objectiveselected_info.getText();
		System.out.println(text);
	}

	@Step("User select the Follow-Up option ,  Method: {method} ")
	public void followUp() {
		boolean followUp = false;
		if (followUp == true)
			followup.click();

	}

	@Step("Click Save Button,  Method: {method} ")
	public void saveButton() {
		SaveButton.click();
	}

	@Step("Click Publish Button,  Method: {method} ")
	public void publishButton() {
		PublishButton.click();
	}

	@Step("Click Show Additional Settings Button,  Method: {method} ")
	public void ShowAdditionalSettingsButton() {
		ShowAdditionalSettingsButton.click();
	}

	@Step("Verify Warm-Up or Follow-Up Type is displayed as expected,  Method: {method} ")
	public void warmUp_Questions() {

		List<WebElement> QuestionList = driver
				.findElements(By
						.xpath("//table[@class='table table-bordered table-condensed table-hover']/tbody/tr"));
		for (int i = 0; i < QuestionList.size(); i++) {
			List<WebElement> questionTitle = QuestionList.get(i).findElements(
					By.xpath("//td[@class='textIndentLeft']"));
			for (int j = 0; j < questionTitle.size(); j++) {
				String value = questionTitle.get(j).getText();
				System.out.println(value);

			}
		}

	}

	@Step("Verify Created Assignment is displayed in Assignment Title Section,  Method: {method} ")
	public void Assignmentlinks(String AssignmentName) {
		List<WebElement> links = driver.findElements(By
				.xpath("//div[@id='activity_list_table_wrapper']/table/tbody"));
		int linkCount = links.size();
		for (int i = 0; i < links.size(); i++) {
			List<WebElement> linkstext = links.get(i).findElements(
					By.xpath("//tr/td[@class=' title_td']/a"));
			for (int j = 0; j < linkstext.size(); j++) {
				String AssignmentText = linkstext.get(j).getText();
				if (AssignmentText.equalsIgnoreCase(AssignmentName)) {
					System.out.println(AssignmentText);
					linkstext.get(i).click();
				}
			}

		}

	}

	@Step("User select the GAU Time,  Method: {method} ")
	public void selectGAUTime(String time) {
		time = timetoselect.getTime().toString();
		System.out.println(time);
		Select oselect = new Select(SelectTime);
		oselect.selectByVisibleText(time);
	}

	@Step("User select the Ungraded Practice,  Method: {method} ")
	public void selectUngradedPractice(String practiceName) {
		Select oselect1 = new Select(SelectUngradedPractice);
		oselect1.selectByVisibleText(practiceName);
		LogUtil.log("The Ungraded Practice Name " + practiceName);
	}

	@Step("User select the Show Solution,  Method: {method} ")
	public void selectshowsolution(String solutionName) {
		Select oselectsolName = new Select(SelectShowsolution);
		oselectsolName.selectByVisibleText(solutionName);
		LogUtil.log("The Show Solution Name " + solutionName);
	}

	@Step("User select the Attempts from Question Level Settings,  Method: {method} ")
	public void selectAttempts(String AttemptsCount) {

		SelectAttempts.click();
		List<WebElement> attemptMenuItem = driver
				.findElements(By
						.xpath("//ul[@class='ant-select-dropdown-menu ant-select-dropdown-menu-vertical  ant-select-dropdown-menu-root']/li"));
		for (WebElement liopt : attemptMenuItem) {
			AttemptsCount = liopt.getText();
			System.out.println(AttemptsCount);
			if (AttemptsCount.equals("3")) {
				liopt.click();
				return;
			}
			LogUtil.log("The Attempts from Question Level Settings "
					+ AttemptsCount);

		}
		throw new NoSuchElementException("Can't find " + attemptMenuItem
				+ " in dropdown");

	}

	/**
	 * @param showStudentScore
	 */
	@Step("User select the Show Student Score in,  Method: {method} ")
	public void selectshowstudentscore(String showStudentScore) {
		// TODO Auto-generated method stub
		Select oselectstudentscore = new Select(SelectShowstudentscore);
		oselectstudentscore.selectByVisibleText(showStudentScore);
		LogUtil.log("The Show Student Score In: " + showStudentScore);
	}
	
	
	/*@Step("User select the Date,  Method: {method} ")
	public void selectDate(String day) {
		 WebElement datepicker = driver.findElement(By.xpath("//table[@class='ant-calendar-table']/tbody[@class='ant-calendartbody']/tr/td/span[contains(text(),'"+day+"')]"));
		 datepicker.click();
		 
	}*/
	
	@Step("User select the Date,  Method: {method} ")
	public void selectDate(String day) {
		 List<WebElement> datepicker = driver.findElements(By.xpath("//table[@class='ant-calendar-table']/tbody[@class='ant-calendartbody']/tr/td/span"));
		 for(WebElement ele : datepicker){
			String date =ele.getText();
			 if(date.equalsIgnoreCase(day)){
				 ele.click();
				 break;
			 }
		 }
		 
		 
	}
	@Step("User select the  GAU Date,  Method: {method} ")
	public void selectGAUDate() throws ParseException{
		
		String dot="11/15/2019";
		
		System.out.println(dot);
		String date,month,year;
		String caldt,calmonth,calyear;	
		String dateArray[]=dot.split("/");
		month=dateArray[0];
		date=dateArray[1];
		year=dateArray[2];
		driver.findElement(By.xpath("//input[@placeholder='mm/dd/yyyy']")).click();
		WebElement cal;
		cal=driver.findElement(By.className("ant-calendar"));
		calyear=driver.findElement(By.className("ant-calendar-year-select")).getText();
		System.out.println(calyear);
		/**
		 * Select the year
		 */
		while (!calyear.equals(year)) 
		{
			driver.findElement(By.className("ant-calendar-year-panel-year")).click();
			calyear=driver.findElement(By.className("ant-calendar-year-panel-year")).getText();
			System.out.println("Displayed Year::" + calyear);
		}
		
		calmonth=driver.findElement(By.className("ant-calendar-month-select")).getText();
		System.out.println(calmonth);
		SimpleDateFormat inputFormat = new SimpleDateFormat("MMMM");
		Calendar cal1 = Calendar.getInstance();
		
		cal1.setTime(inputFormat.parse(calmonth));
		SimpleDateFormat outputFormat = new SimpleDateFormat("MM"); // 01-12
		String calMonthNum =(outputFormat.format(cal1.getTime()));
		
		if (!calMonthNum.equalsIgnoreCase(month)) 
		{
			driver.findElement(By.className("ant-calendar-next-month-btn")).click();
			calmonth=driver.findElement(By.className("ant-calendar-month-select")).getText();
			System.out.println("Displayed Month::" + calmonth);
		} else {
			System.out.println("Displayed Month::" + calmonth);
		}
		
		cal=driver.findElement(By.className("ant-calendar"));
		/**
		 * Select the Date
		 */
		List<WebElement> rows,cols;
		rows=cal.findElements(By.tagName("tr"));
	  
		for (int i = 1; i < rows.size(); i++) 
		{
			cols=rows.get(i).findElements(By.tagName("td"));
		
			for (int j = 0; j < cols.size(); j++) 
			{
				
				caldt=cols.get(j).getText();
				if (caldt.equals(date)) {
					cols.get(j).click();
					break;
				}
			}
		}
}

	}
	


