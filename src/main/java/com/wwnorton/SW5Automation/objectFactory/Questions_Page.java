/**
 * 
 */
package com.wwnorton.SW5Automation.objectFactory;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import ru.yandex.qatools.allure.annotations.Step;

/**
 * @author Shripad
 *
 * 27-Sep-2019
 */
public class Questions_Page {
	
WebDriver driver;

	@FindBy(how =How.XPATH, using="//button[@class='btn-submit-s btn-primary lato-regular-14']/span[contains(text(),'ADD QUESTIONS')]")
	public WebElement AddQuestionButton;
	
	@FindBy(how =How.XPATH, using="//div[@class='dropdown-select']/div/span[contains(text(),'TYPE')]")
	public WebElement TypeListBox;
	
	@FindBy(how =How.XPATH, using="//span[@class='ant-tree-title'][contains(.,'Numeric Entry')]")
	public WebElement NumericEntry_checkbox;
	
	@FindBy(how =How.XPATH, using="//span[@class='glyphicons.glyphicons-plus']")
	public WebElement QuestionplusIcon;
	
	@FindBy(how =How.XPATH, using="//div[@class='head-box']/div[@class='head-title fl']/a")
	public WebElement HeadTitleLink;
	
	public Questions_Page(WebDriver driver){
		
		this.driver = driver;
		PageFactory.initElements(driver, this);

	}
	@Step("Click on the Add Questions Button,  Method: {method} ")
	public void addQuestions(){
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].scrollIntoView();", AddQuestionButton);
		AddQuestionButton.click();
		
	}
	
	@Step("Select the Numeric Entry from the Type List Box ,  Method: {method} ")
	public void SelectTypeListbox() throws InterruptedException{
		Thread.sleep(3000);
		TypeListBox.click();
	     /* Actions builder = new Actions(driver);
	      builder.moveToElement(element).perform();*/
		Thread.sleep(5000);
	      NumericEntry_checkbox.click();
	      driver.findElement(By.xpath("//input[@type='text']")).click();
	      Thread.sleep(5000);
	      questionsLibraryList();
	      
	}
	
	@Step("Click the Head Title Link ,  Method: {method} ")
	public void headTitleLink() throws InterruptedException{
		
		HeadTitleLink.click();

		
	      
	}
	
	
	public void questionsLibraryList(){
		List<WebElement> questionslibList = driver.findElements(By.xpath("//table[@class='newQlPageTable table table-bordered table-condensed table-hover']/tbody/tr/td/span"));
		for(int i=0; i<questionslibList.size(); i++){
			 if(i==1){
				 questionslibList.get(i).click();
			 }
		
		}
	}
		public void questionsLibraryList_add() throws InterruptedException{
			List<WebElement> questionslibList = driver.findElements(By.xpath("//table[@class='newQlPageTable table table-bordered table-condensed table-hover']/tbody/tr/td/span"));
			for(int i=0; i<questionslibList.size(); i++){
				questionslibList.get(i).click();
				Thread.sleep(2000);
					if(i==6) {
						break;
				}
			
			}
	}
	
	
	
	
}
